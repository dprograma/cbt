<?php
require('sys/config/config.php');
date_default_timezone_set('UTC');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Date Form</title>
</head>
<body>
<form method="post" action="<?php echo basename($_SERVER['PHP_SELF']); ?>">
	<table><tr><td><label for="timefrom">Time From</label></td><td><input type="date" name="timefrom" placeholder="<?php echo date('h:s:i a')?>"></td></tr><tr><td><label for="timeto">Time To</label></td><td><input type="date" name="timeto" placeholder="<?php echo date('h:s:i a')?>"></td></tr>
	<tr><td></td><td><input type="submit" name="action" value="submit"></td></table>
</form>
</body>
</html>
<?php
if(isset($_POST['timefrom']) || isset($_POST['timeto'])){
$timefrom = strtotime(trim(strip_tags($_POST['timefrom'])));
$timeto = strtotime(trim(strip_tags($_POST['timeto'])));

$starttime = date('h:i:s a', $timefrom);
$stoptime = date('h:i:s a', $timeto);
$difference = ($timeto-$timefrom);
$duration = date('h:i:s',$difference);

echo "Time From: " . $starttime . "<br/>";
echo "Time To: " .$stoptime . "<br/>";
echo "Duration is: " . $duration;
}
?>