<?php
session_start();
 if(isset($_SESSION['admin']) || isset($_SESSION['teacher'])) {
	
?>            
<!DOCTYPE html>
<html>
<head>
	<?php
	if (isset($_SESSION['admin_usr_name']) && isset($_SESSION['admin_pwd']) && isset($_SESSION['cryption'])){
	$admin_username = $_SESSION['admin_usr_name'];
	$admin_password = $_SESSION['admin_pwd'];
	$crypt	= $_SESSION['cryption'];
	}
	elseif (isset($_SESSION['teacher_usr_name']) && isset($_SESSION['teacher_pwd']) && isset($_SESSION['teacher_cryption'])) {
	$teacher_username = $_SESSION['teacher_usr_name'];
	$teacher_password = $_SESSION['teacher_pwd'];
	$t_crypt	= $_SESSION['teacher_cryption'];
	}
	?>
	<title>
	<?php
	if(isset($admin_username) || isset($admin_password)) {
	echo "Admin Dashboard";
	}

	elseif(isset($teacher_username) || isset($teacher_password)) {
	echo "Teacher Dashboard";
	}
	?>
	</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../files/css/styles.css">
	<script type="text/javascript" src="../files/js/jquery-3.1.1.js"></script>

</head>
<body class="no_background">
<?php
include('admin_header.php');
include('admin_menus.php');
?>
<section class="main_area">
<?php
echo "<table class='form_table'><tr>";
	if (isset($admin_username) || isset($admin_password)) {
		$len = count($admin_button_menus);
		$rows = ceil($len/4);
		$cols = $len/$rows;
		for ($i=0; $i < $rows; $i++) { 
			$s = $i*$cols;
			echo "</tr><tr>";
			for ($j=$s; $j < $s+$cols; $j++) { 
				if($admin_button_menus[$j] == 'Log Out') {
				echo "<td><a id = 'button_link' href ='../index.php?action=Logout'>" . $admin_button_menus[$j] . "</a></td>";
				}
				else{
				echo "<td><a id = 'button_link' href ='" . preg_replace('/\s+/', '_', strtolower($admin_button_menus[$j])) . ".php'>" . $admin_button_menus[$j] . "</a></td>";
				}
			}
		}
	}
	elseif (isset($teacher_username) || isset($teacher_password)) {
		$len = count($teacher_button_menus);
		$rows = ceil($len/3);
		$cols = $len/$rows;
		for ($i=0; $i < $rows; $i++) { 
			$s = $i*$cols;
			echo "</tr><tr>";
			for ($j=$s; $j < $s+$cols; $j++) { 
				if($teacher_button_menus[$j] == 'Log Out') {
				echo "<td><a id = 'button_link' href ='../index.php?action=Logout'>" . $teacher_button_menus[$j] . "</a></td>";
				}
				else{
				echo "<td><a id = 'button_link' href ='" . preg_replace('/\s+/', '_', strtolower($teacher_button_menus[$j])) . ".php'>" . $teacher_button_menus[$j] . "</a></td>";
				}
			}
		}
	}
echo "</tr></table>";
?>
</section>
</body>
</html>
<?php
}
?>