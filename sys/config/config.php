<?php
//connect to mysql server
    $conn = mysqli_connect("localhost","root","");
    
//create database cbt on mysql server
    $sql = "CREATE DATABASE IF NOT EXISTS cbt DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci";
    mysqli_query($conn,$sql);
    
//select current database
    mysqli_select_db($conn,'cbt');
    
//Create Admin Table
    $sql = "CREATE TABLE IF NOT EXISTS adminTable(
            adminId INT(10)NOT NULL auto_increment,
            adminPwd BLOB NOT NULL,
            encrypt VARCHAR(150) NOT NULL,
            adminUserName BLOB NOT NULL,
            adminPrivilege INT(5)NOT NULL default 1,
            PRIMARY KEY(adminId))ENGINE=InnoDB;";
    mysqli_query($conn,$sql);
    
//Create Teacher Table

    $sql = "CREATE TABLE IF NOT EXISTS teacherTable(
            teacherId INT(10)NOT NULL auto_increment,
            teacherUserName VARCHAR(50) NOT NULL,
            teacherPwd BLOB NOT NULL,
            encrypt VARCHAR(150) NOT NULL,
            teacherPrivilege INT(5)NOT NULL default 2,
            admin_Id INT(10),
            PRIMARY KEY(teacherId),
            INDEX adm_ind(admin_Id),
            CONSTRAINT FOREIGN KEY adm_fk_tec(admin_Id) REFERENCES adminTable(adminId) 
            ON UPDATE CASCADE ON DELETE CASCADE)ENGINE = InnoDB;";
    mysqli_query($conn,$sql);
    
//Create Student Table
    
    $sql = "CREATE TABLE IF NOT EXISTS studentTable(
            studentId VARCHAR(20)NOT NULL,
            studentName VARCHAR(20) NOT NULL,
            studentPwd BLOB NOT NULL,
            encrypt VARCHAR(150) NOT NULL,
            className VARCHAR(50) NOT NULL,
            studentPrivilege INT(1)NOT NULL default 3,
            admin_Id INT(10),
            teacher_Id INT(10),
            PRIMARY KEY(studentId),
            INDEX adm_index_stu(admin_Id),
            INDEX tec_index_stud(teacher_Id),
            CONSTRAINT FOREIGN KEY adm_fk_stu(admin_Id) REFERENCES adminTable(adminId) 
            ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT FOREIGN KEY tec_fk_stu(teacher_Id) REFERENCES teacherTable(teacherId) 
            ON UPDATE CASCADE ON DELETE CASCADE) ENGINE = InnoDB;";
    mysqli_query($conn,$sql);

//Create Subject Table
    
    $sql = "CREATE TABLE IF NOT EXISTS subjectTable(
            subjectId INT(10)NOT NULL auto_increment,
            subjectName VARCHAR(100)NOT NULL,
            className VARCHAR(50) NOT NULL,
            admin_Id INT(10), 
            teacher_Id INT(10),
            student_Id VARCHAR(20),
            PRIMARY KEY(subjectId),
            INDEX idx_adm_sub(admin_Id),
            INDEX idx_teacher_sub(teacher_Id),
            INDEX idx_student_sub(student_Id),
            CONSTRAINT FOREIGN KEY adm_fk_sub(admin_Id) REFERENCES adminTable(adminId) 
            ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT FOREIGN KEY tec_fk_sub(teacher_Id) REFERENCES teacherTable(teacherId) 
            ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT FOREIGN KEY stu_fk_sub(student_Id) REFERENCES studentTable(studentId) 
            ON UPDATE CASCADE ON DELETE CASCADE) ENGINE = InnoDB;";
    mysqli_query($conn,$sql);
    
//Create Exam Table
    
    $sql = "CREATE TABLE IF NOT EXISTS examTable(
            examId INT(10)NOT NULL auto_increment,
            examName VARCHAR(100)NOT NULL,
            admin_Id INT(10), 
            teacher_Id INT(10),
            student_Id VARCHAR(20),
            subject_Id INT(10),
            PRIMARY KEY(examId),
            INDEX idx_adm_exa(admin_Id),
            INDEX idx_teacher_exa(teacher_Id),
            INDEX idx_student_exa(student_Id),
            INDEX idx_subject_exa(subject_Id),
            CONSTRAINT FOREIGN KEY adm_fk_exa(admin_Id) REFERENCES adminTable(adminId) 
            ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT FOREIGN KEY tec_fk_exa(teacher_Id) REFERENCES teacherTable(teacherId) 
            ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT FOREIGN KEY stu_fk_exa(student_Id) REFERENCES studentTable(studentId) 
            ON UPDATE CASCADE ON DELETE CASCADE, 
            CONSTRAINT FOREIGN KEY sub_fk_exa(subject_Id) REFERENCES subjectTable(subjectId) 
            ON UPDATE CASCADE ON DELETE CASCADE) ENGINE = InnoDB;";
    mysqli_query($conn,$sql);

//Create Class Table

    $sql = "CREATE TABLE IF NOT EXISTS classTable(
            classId INT(10)NOT NULL auto_increment,
            className VARCHAR(100)NOT NULL,
            student_Id VARCHAR(20),
            PRIMARY KEY(classId),
            INDEX idx_student_cla(student_Id),
            CONSTRAINT FOREIGN KEY stu_fk_cla(student_Id) REFERENCES studentTable(studentId) 
            ON UPDATE CASCADE ON DELETE CASCADE) ENGINE = InnoDB;";
    mysqli_query($conn,$sql);

//Create Time Table

    $sql = "CREATE TABLE IF NOT EXISTS timeTable(
            Id INT(10)NOT NULL auto_increment,
            subjectName VARCHAR(100)NOT NULL,
            className VARCHAR(100) NOT NULL,
            examName VARCHAR(100) NOT NULL,
            examDAy DATE,
            startTime TIME,
            stopTime TIME,
            duration TIME default '00:00:00',
            admin_Id INT(10), 
            teacher_Id INT(10),
            PRIMARY KEY(Id),
            INDEX idx_adm_tim(admin_Id),
            INDEX idx_teacher_tim(teacher_Id),
            CONSTRAINT FOREIGN KEY adm_fk_tim(admin_Id) REFERENCES adminTable(adminId) 
            ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT FOREIGN KEY tec_fk_tim(teacher_Id) REFERENCES teacherTable(teacherId) 
            ON UPDATE CASCADE ON DELETE CASCADE) ENGINE = InnoDB;";
    mysqli_query($conn,$sql);

    $sql = "CREATE TABLE IF NOT EXISTS questionTable(
            questionId INT(10) NOT NUL auto_increment,
            question TEXT NOT NULL,
            optionA VARCHAR(200) NOT NULL,
            optionB VARCHAR(200) NOT NULL,
            optionC VARCHAR(200) NOT NULL,
            optionD VARCHAR(200) NOT NULL,
            optionE VARCHAR(200) NOT NULL,
            PRIMARY KEY(questionId)
    )ENGINE = InnoDB;";
?>