<?php
session_start();
date_default_timezone_set('UTC');
if(isset($_SESSION['admin']) && isset($_SESSION['admin_usr_name']) && isset($_SESSION['admin_pwd'])) {              
?>
<!DOCTYPE html>
<html>
<head>
	<title>Set Exam Time Table</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
    <script type="text/javascript" src="../files/js/jquery-3.3.1.js"></script>
    <script type="text/javascript">
		function getPage(sub){
			$.ajax({
				url:"../index.php?action=display",
				type:"post",
				data:'sub='+sub,
				datatype:"html",
				success:function(response){
					$(".subjectclass").empty();
					$(".subjectclass").html(response);
				}
			});
		}
</script>

</head>
<body style = "position: absolute !important;" class="no_background">
<?php
include('admin_header.php');
include('admin_menus.php');
require('config/config.php');
?>
<form class="main_div" method="post" action="../index.php" onSubmit = 'submitTimeTable(event);'>
<div class="opanel">
	<div class="exampanel">
		<table class="spacing"><caption>Select Exam</caption><tr><td><select name = 'selectexam' class = 'examclass'>
			<?php
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_prepare($stmt,"SELECT examName FROM examTable");
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				mysqli_stmt_bind_result($stmt,$exam_name);
				while (mysqli_stmt_fetch($stmt)) {
					echo "<option value = '$exam_name'>" . $exam_name . "</option>";
				}
			?>
		</select></td></tr></table>
	</div>
	<div class="exampanel">
		<table class="dateborder"><caption>Select Date</caption><tr><td>
			<input type="date" name="sl_date" placeholder="<?php echo date('Y/m/d'); ?>">
		</td></tr></table>
	</div>
	<div class="classpanel">
		<table class="spacing"><caption>Select Class</caption>
			<?php
				mysqli_stmt_prepare($stmt,"SELECT className FROM classTable");
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				mysqli_stmt_bind_result($stmt,$class_name);
				while (mysqli_stmt_fetch($stmt)) {
					echo "<tr><td><a id = 'classclass' class = '" . preg_replace('/\s+/', '_', $class_name) . "' href = '#'>" . $class_name . "</a></td></tr>";
				}
			?>
			<script type="text/javascript">
				$(document).ready(function(){
					$('.subtable').empty();
					$('.JSS_1').click(function(){
						$.ajax({
							url:"../index.php?action=JSS1",
							type:"post",
							datatype:"html",
							data:"class=JSS1",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.JSS_2').click(function(){
						$.ajax({
							url:"../index.php?action=JSS2",
							type:"post",
							datatype:"html",
							data:"class=JSS2",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.JSS_3').click(function(){
						$.ajax({
							url:"../index.php?action=JSS3",
							type:"post",
							datatype:"html",
							data:"class=JSS3",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.SSS_1').click(function(){
						$.ajax({
							url:"../index.php?action=SSS1",
							type:"post",
							datatype:"html",
							data:"class=SSS1",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.SSS_2').click(function(){
						$.ajax({
							url:"../index.php?action=SSS2",
							type:"post",
							datatype:"html",
							data:"class=SSS2",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.SSS_3').click(function(){
						$.ajax({
							url:"../index.php?action=SSS3",
							type:"post",
							datatype:"html",
							data:"class=SSS3",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
				});

				function submitTimeTable(e){
					e.preventDefault();
					var timeForm = $('form')[0];
					var tmForm = new FormData(timeForm);
					$.ajax({
						url: '../index.php?action=Submit',
						data: tmForm,
						type: 'post',
						contentType: false,
						processData: false,
						success: function(){
							$(".successdiv").css({
								'position': 'relative',
						 		'top': '85px',
						 		'left': '610px',
						 		'width': '250px',
						 		'height': '70px',
						 		'border-radius': '2px',
						 		'background-color': 'rgba(0,255,0,0.4)',
						 		'opacity': '0.5',
						 		'color': 'black',
						 		'font-size': '14px',
						 		'text-align': 'center',
						 		'overflow': 'hidden',
						 		'margin': '0',
						 		'padding': '0',
						 		'border': '0',
						 		'display': 'block',
						 		'line-height': '70px'
						 		}).html('Successful Entry!').fadeIn().delay(3000).fadeOut();
						}
					});
				}
			</script>
		</table>
	</div>	
</div>
<div class="dpanel">
	<table class="subtable"></table>
	<table class="subjectclass"></table>
</div>
</form>
<div class='successdiv'> 
</body>
</html>
<?php
} else {
	$url = "admin.php";
	header("Location:$url");
}
?>
