<?php
session_start();
if (isset($_SESSION['admin_usr_name']) && isset($_SESSION['admin_pwd']) && isset($_SESSION['cryption'])&& isset($_SESSION['admin'])) {
    
?>
<html>
    <head>
        <title>Create New Teacher</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
        <script type="text/javascript" src="../files/js/jquery-3.1.1.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".close_error_div_img").click(function(){
                    $(".login_error_div").fadeOut();
                });

                $(".login_error_div").fadeIn().delay(5000).fadeOut();

                $(".close_success_div_img").click(function(){
                    $(".reg_success_div").fadeOut();
                });

                $(".reg_success_div").fadeIn().delay(5000).fadeOut();

            });
        </script>
    </head>
    <body class="no_background">
        <?php 
        $admin_username = $_SESSION['admin_usr_name'];
        $admin_password = $_SESSION['admin_pwd'];
        $crypt = $_SESSION['cryption'];

        if (isset($_SESSION['teacher_login_error'])) {
            $login_error = $_SESSION['teacher_login_error'];
            unset($_SESSION['teacher_login_error']);
        }
         
         if (isset($_SESSION['reg_success_report'])) {
            $success_report = $_SESSION['reg_success_report'];
            unset($_SESSION['reg_success_report']);
        }
         
        include('admin_header.php');
        include('admin_menus.php');

        if(isset($login_error)){
            echo "<div id ='err' class='login_error_div'>" . $login_error . "<img class='close_error_div_img' src='../files/images/error.png'></div>";
        }   
        if(isset($success_report)){
            echo "<div id='suc' class='reg_success_div'>" . $success_report . "<img class='close_success_div_img' src='../files/images/success.png'></div>";
        }         
        ?>
            <form id="teacher_registration_form" method="post" action="../index.php">
                <table><tr><td><label>User Name</label></td><td><input id="teacher_user_name" name="teacher_user_name" type="text" placeholder="User Name" size="50"></td></tr>
                <tr><td><label>Password</label></td><td><input id="teacher_password" name="teacher_password" type="password" placeholder="Password" size="50"></td></tr>
                <tr><td><label>Re-enter Password</label></td><td><input id="teacher_reenter_password" name="teacher_reenter_password" type="password" placeholder="Re-enter Password" size="50"></td></tr>
                <tr><td></td><td><input class="register_button" id = "action" name="action" type="submit" value="Register Teacher"></td></tr>
                </table>
            </form>
        
        <?php require('../files/footer.php'); ?>
    </body>
</html>
<?php
}
?>