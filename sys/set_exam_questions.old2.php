<?php
session_start();
if (isset($_SESSION['teacher_usr_name']) && isset($_SESSION['teacher_pwd']) && isset($_SESSION['teacher_cryption'])) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Examination</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
    <script type="text/javascript" src="../files/js/jquery-3.3.1.js"></script>		
	
    <script type="text/javascript">

		function uploadquestion(){
    		var img = document.getElementById("imagequestion");
			document.getElementById("img1").value = img.value.split('\\').pop();
    	}

    	function uploadoptionone(){
    		var img = document.getElementById("imageoptionA");
			document.getElementById("img2").value = img.value.split('\\').pop();
    	}
		
		function uploadoptiontwo(){
			var img = document.getElementById("imageoptionB");
			document.getElementById("img3").value = img.value.split('\\').pop();
		}
		
		function uploadoptionthree(){
			var img = document.getElementById("imageoptionC");
			document.getElementById("img4").value = img.value.split('\\').pop();
		}
		
		function uploadoptionfour(){
			var img = document.getElementById("imageoptionD");
			document.getElementById("img5").value = img.value.split('\\').pop();
		}
		
		function uploadoptionfive(){
			var img = document.getElementById("imageoptionE");
			document.getElementById("img6").value = img.value.split('\\').pop();
		}	
	</script>

    <script type="text/javascript">

    	function QuestionNumber(a){
    		var num = $('#noofquestions').find(':selected').val();
    		var year = $('#year').find(":selected").val();
    		var exam = $('#exam').find(":selected").val();
    		var theclass = $('#class').find(":selected").val();
    		var subject = $('#subject').find(":selected").val();
    		$.ajax({
    			url:"../index.php?action=looper",
    			data:"buttonnumber="+a+"&num="+num+"&year="+year+"&exam="+exam+"&class="+theclass+"&subject="+subject,
    			type:"post",
    			datatype:"html",
    			success: function(response){
    				$("#page").html(response);
    			}
    		});
    	}

    	function displayPage(){
    		var num = $('#noofquestions').find(':selected').val();
    		var year = $('#year').find(":selected").val();
    		var exam = $('#exam').find(":selected").val();
    		var theclass = $('#class').find(":selected").val();
    		var subject = $('#subject').find(":selected").val();
    		$.ajax({
    			url:"../index.php?action=tabs",
					type:"post",
					data:'num='+num+'&year='+year+'&exam='+exam+'&class='+theclass+'&subject='+subject,
					datatype:"html",
					success:function(response){
						$("#page").html(response);
					}
    		});
    	}
		
    </script> 		
</head>
<body style = "position: absolute !important;" class="no_background">
	<?php
		require('config/config.php');
		include('admin_header.php');
		include('admin_menus.php');
		
	?>
	<script type="text/javascript">
		function submitquestion(){
			if ($('#imagequestion').val())
			 {
			 	$('#questionbox').prop('required',false);
			 }
			 else{
			 	$('#questionbox').prop('required',true);
			 }
			 var formvl = $('form').serialize;
			$.ajax({
				url:"../index.php?action=looper",
				data: formvl,
				datatype:"json",
				type:"post",
				success: function(response){
					$("#page").html(response);
				}
			});
		}
	</script>
	<form method="post" action="../index.php">
		<?php
			$list = array('year','exam','class','subject','noofquestions');
		?>
		<ul class="topmenuitems"><?php 
				foreach($list as $itm){
					if ($itm == 'year') {
						$style = "style = 'display:block;'";
					}
					elseif($itm == 'noofquestions'){
						$style = "style = 'display:none;' onChange = 'displayPage()'";
					}
					else{
						$style = "style = 'display:none;'";
					}
					echo "<li><label " . $style . " for = '$itm' >" . "Select " . $itm . "</label><select class = 'examclass' " . $style . " id = '" . $itm . "' name = '" . $itm . "'><option selected>" . "Select " . $itm;
            			if($itm == 'year'){
            				$year = date('Y');
            				echo "<option value ='" . $year . "'>" . $year . "</option>";
            			}
            			elseif($itm == 'noofquestions') {
            				for ($i=10; $i <= 120; $i+=10) { 
            					echo "<option value ='" . $i . "'>" . $i . "</option>";
            				}
            			}
            			else{
						$stmt = mysqli_stmt_init($conn);
						$sql = "SELECT DISTINCT " . $itm . "Name FROM " . $itm . "Table";
						mysqli_stmt_prepare($stmt,$sql);
            			mysqli_stmt_execute($stmt);
            			mysqli_stmt_store_result($stmt);
            			mysqli_stmt_bind_result($stmt,$itmname);
            			$row = mysqli_stmt_num_rows($stmt);
            				if($row>0){
            					while (mysqli_stmt_fetch($stmt)) {
					
								echo "</option><option value ='" . $itmname . "'>" . $itmname . "</option>";
            					}
            				}
            			}
            		echo "</select></li>";
				}
			?></ul>
		<script type = 'text/javascript'>
			$(document).ready(function(){
		<?php
			foreach ($list as $l) {
				$next = next($list);
			?>	
			
			$('#<?php echo $l; ?>').change(function(){
				var next = <?php echo json_encode($next); ?>;
					$('#<?php echo $next; ?>').fadeIn(1000);
					$("label[for='<?php echo $next; ?>']").fadeIn();
				});
		<?php
			}
		?>
			});
		</script>
	</form>
	<div id="page"></div>
</body>
</html>
<?php
}
else{
	$url = "../index.php";
	header("location:$url");
}