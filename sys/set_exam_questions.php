<?php
session_start();
if (isset($_SESSION['teacher_usr_name']) && isset($_SESSION['teacher_pwd']) && isset($_SESSION['teacher_cryption'])) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Examination</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
    <script type="text/javascript" src="../files/js/scripts.js"></script> 
    <script type="text/javascript" src="../files/js/jquery-3.3.1.js"></script>	
    <script type="text/javascript">
    	var SubmitOk = "True";
		function submitquestion(){
			if ($('#imagequestion').val())
			 {
			 	$('#questionbox').prop('required',false);
			 	SubmitOk = "True";
			 }
			 else{
			 	$('#questionbox').prop('required',true);
			 	SubmitOk = "False";
			 }

			 if ($('#imageoptionA').val())
			 {
			 	$('#optionA').prop('required',false);
			 	SubmitOk = "True";
			 }
			 else{
			 	$('#optionA').prop('required',true);
			 	SubmitOk = "False";
			 }

			 if ($('#imageoptionB').val())
			 {
			 	$('#optionB').prop('required',false);
			 	SubmitOk = "True";
			 }
			 else{
			 	$('#optionB').prop('required',true);
			 	SubmitOk = "False";
			 }

			 if ($('#imageoptionC').val())
			 {
			 	$('#optionC').prop('required',false);
			 	SubmitOk = "True";
			 }
			 else{
			 	$('#optionC').prop('required',true);
			 	SubmitOk = "False";
			 }

			 if ($('#imageoptionD').val())
			 {
			 	$('#optionD').prop('required',false);
			 	SubmitOk = "True";
			 }
			 else{
			 	$('#optionD').prop('required',true);
			 	SubmitOk = "False";
			 }
			 var $radio = $('input:radio[name = questOpt]');
			 if ($radio.is(':checked') === true) {
			 	$radio.prop('required',false);
			 	SubmitOk = "True";
			 }
			 else{
			 	$radio.prop('required',true);
			 	SubmitOk = "False";
			 }
			 if (SubmitOk == "True") {
			 	
			 	var myform = $('form')[1];
			 	var formData = new FormData(myform);
			 	formData.append('sbf','sb');
				$.ajax({
					url:"../index.php?action=looper",
					data: formData,
					type:"post",
					contentType: false,
					processData: false,
					success: function(response){
						$("#page").html(response);
						$('.successentry').css({
						'position': 'relative',
				 		'top': '170px',
				 		'left': '610px',
				 		'width': '250px',
				 		'height': '70px',
				 		'border-radius': '2px',
				 		'background-color': 'rgba(0,255,0,0.4)',
				 		'opacity': '0.5',
				 		'color': 'black',
				 		'font-size': '14px',
				 		'text-align': 'center',
				 		'overflow': 'hidden',
				 		'margin': '0',
				 		'padding': '0',
				 		'border': '0',
				 		'display': 'block',
				 		'line-height': '70px'
				 		}).html('Successful Entry!').fadeIn().delay(5000).fadeOut();

					}
				});
			 }
			 
		}
	

    </script>	  
</head>
<body style = "position: absolute !important;" class="no_background">
	<?php
		require('config/config.php');
		include('admin_header.php');
		include('admin_menus.php');
		
	?>
	
	<form method="post" action="../index.php">
		<?php
			$list = array('year','exam','class','subject','noofquestions');
		?>
		<ul class="topmenuitems"><?php 
				foreach($list as $itm){
					if ($itm == 'year') {
						$style = "style = 'display:block;'";
					}
					elseif($itm == 'noofquestions'){
						$style = "style = 'display:none;' onChange = 'displayPage()'";
					}
					else{
						$style = "style = 'display:none;'";
					}
					echo "<li><label " . $style . " for = '$itm' >" . "Select " . $itm . "</label><select class = 'examclass' " . $style . " id = '" . $itm . "' name = '" . $itm . "'><option selected>" . "Select " . $itm;
            			if($itm == 'year'){
            				$year = date('Y');
            				echo "<option value ='" . $year . "'>" . $year . "</option>";
            			}
            			elseif($itm == 'noofquestions') {
            				for ($i=10; $i <= 120; $i+=10) { 
            					echo "<option value ='" . $i . "'>" . $i . "</option>";
            				}
            			}
            			else{
						$stmt = mysqli_stmt_init($conn);
						$sql = "SELECT DISTINCT " . $itm . "Name FROM " . $itm . "Table";
						mysqli_stmt_prepare($stmt,$sql);
            			mysqli_stmt_execute($stmt);
            			mysqli_stmt_store_result($stmt);
            			mysqli_stmt_bind_result($stmt,$itmname);
            			$row = mysqli_stmt_num_rows($stmt);
            				if($row>0){
            					while (mysqli_stmt_fetch($stmt)) {
					
								echo "</option><option value ='" . $itmname . "'>" . $itmname . "</option>";
            					}
            				}
            			}
            		echo "</select></li>";
				}
			?></ul>
		<script type = 'text/javascript'>
			$(document).ready(function(){
		<?php
			foreach ($list as $l) {
				$next = next($list);
			?>	
			
			$('#<?php echo $l; ?>').change(function(){
				var next = <?php echo json_encode($next); ?>;
					$('#<?php echo $next; ?>').fadeIn(1000);
					$("label[for='<?php echo $next; ?>']").fadeIn();
				});
		<?php
			}
		?>
			});
		</script>
	</form>
	<div id="page"></div>
	<div class = 'successentry'></div>

</body>
</html>
<?php
}
else{
	$url = "../index.php";
	header("location:$url");
}