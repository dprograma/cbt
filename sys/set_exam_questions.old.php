<?php
session_start();
if (isset($_SESSION['teacher_usr_name']) && isset($_SESSION['teacher_pwd']) && isset($_SESSION['teacher_cryption'])) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Examination</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
    <script type="text/javascript" src="../files/js/jquery-3.3.1.js"></script> 
    <script type="text/javascript">
    	function QuestionNumber(a){
    		$.ajax({
    			url:"../index.php?action=looper",
    			data:"buttonnumber="+a,
    			type:"post",
    			datatype:"html",
    			success: function(response){
    				$("#page").empty();
    				$("#page").html(response);
    			}
    		});
    	}

    	function getNext(next){
				var rem = $(this).find(":selected").val();
				console.log(next);
				$.ajax({
					url:"../index.php?action=tabs",
					type:"post",
					data:'num='+rem+'&tab='+next,
					datatype:"html",
					success:function(response){
						$("select[id='"+next+"']").replaceWith(response);
					}
				});
			}

    </script> 		
</head>
<body style = "position: absolute !important;" class="no_background">
	<?php
		require('config/config.php');
		include('admin_header.php');
		include('admin_menus.php');
		
	?>
	<form method="post" action="../index.php">
		<?php
			$yr = date('Y');
			$list = array('exam','class','subject','noofquestions','page');

			$stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,'SELECT examName FROM examTable');

            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$examname);
		?>
		<ul class="topmenuitems"><li><label for="year">Exam Year</label><select class="examclass" id="year" name="year"><option value="<?php echo $yr; ?>"><?php echo $yr; ?></option></select></li><li><label for="exam">Select Exam</label><select class="examclass" id="exam" name="exam" onchange="getNext('class');" required>
			<?php
				while (mysqli_stmt_fetch($stmt)) {
					echo "<option value = '" . $examname . "'>" . $examname . "</option>";
				}
			?>
			</select></li>
			<?php 
				for($itm = 1; $itm < count($list); $itm++) {
					echo "<li><label for = '$list[$itm]'></label><select style = 'display:none;' class = 'examclass' id = '" . $list[$itm] . "' name = '" . $list[$itm] . "'><option></option></select></li>";
				}
			?></ul>		
	</form>
</body>
</html>
<?php
}
else{
	$url = "../index.php";
	header("location:$url");
}