<?php
session_start();
date_default_timezone_set('UTC');
if(isset($_SESSION['admin']) && isset($_SESSION['admin_usr_name']) && isset($_SESSION['admin_pwd'])) {              
?>
<!DOCTYPE html>
<html>
<head>
	<title>Set Exam Time Table</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
    <script type="text/javascript" src="../files/js/jquery-3.3.1.js"></script>
</head>
<body style = "position: absolute !important;" class="no_background">
<?php
include('admin_header.php');
include('admin_menus.php');
require('config/config.php');

if(isset($_SESSION['success_report'])){
$success_report = $_SESSION['success_report'];
unset($_SESSION['success_report']);
}
if(isset($_SESSION['error_report'])){
$error_report = $_SESSION['error_report'];
unset($_SESSION['error_report']);
}
if(isset($success_report)) {
            echo "<div class='success_div'>" . $success_report . "<img class='close_success_div_img' src='../files/images/success.png'></div>";
   }    
if(isset($error_report)) {
            echo "<div class='error_div'>" . $error_report . "<img class='close_error_div_img' src='../files/images/error.png'></div>";
   } 
?>
<form class="main_div" method="post" action="../index.php">
<aside class="opanel">
	<div class="exampanel">
		<table class="spacing"><caption>Select Exam</caption><tr><td><select name = 'selectexam' class = 'examclass'>
			<?php
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_prepare($stmt,"SELECT examName FROM examTable");
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				mysqli_stmt_bind_result($stmt,$exam_name);
				while (mysqli_stmt_fetch($stmt)) {
					echo "<option value = '$exam_name'>" . $exam_name . "</option>";
				}
			?>
		</select></td></tr></table>
	</div>
	<div class="exampanel">
		<table class="dateborder"><caption>Select Date</caption><tr><td>
			<input type="date" name="sl_date" placeholder="<?php echo date('Y/m/d'); ?>">
		</td></tr></table>
	</div>
	<div class="classpanel">
		<table class="spacing"><caption>Select Class</caption>
			<?php
				mysqli_stmt_prepare($stmt,"SELECT className FROM classTable");
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				mysqli_stmt_bind_result($stmt,$class_name);
				while (mysqli_stmt_fetch($stmt)) {
					echo "<tr><td><a id = 'classclass' class = '" . preg_replace('/\s+/', '_', $class_name) . "' href = '#'>" . $class_name . "</a></td></tr>";
				}
			?>
			<script type="text/javascript">
				$(document).ready(function(){
					$(".close_success_div_img").click(function(){
                    $(".success_div").fadeOut();
                	});
                	$(".close_error_div_img").click(function(){
                    $(".error_div").fadeOut();
                });
					$('.subtable').empty();
					$('.JSS_1').click(function(){
						$.ajax({
							url:"../index.php?action=JSS1",
							type:"post",
							datatype:"html",
							data:"class=JSS1",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.JSS_2').click(function(){
						$.ajax({
							url:"../index.php?action=JSS2",
							type:"post",
							datatype:"html",
							data:"class=JSS2",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.JSS_3').click(function(){
						$.ajax({
							url:"../index.php?action=JSS3",
							type:"post",
							datatype:"html",
							data:"class=JSS3",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.SSS_1').click(function(){
						$.ajax({
							url:"../index.php?action=SSS1",
							type:"post",
							datatype:"html",
							data:"class=SSS1",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.SSS_2').click(function(){
						$.ajax({
							url:"../index.php?action=SSS2",
							type:"post",
							datatype:"html",
							data:"class=SSS2",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
					$('.SSS_3').click(function(){
						$.ajax({
							url:"../index.php?action=SSS3",
							type:"post",
							datatype:"html",
							data:"class=SSS3",
							success: function(response){
								$('.subtable').empty();
								$('.subtable').html(response);
							}
						});
					});
				});
			</script>
		</table>
	</div>	
</aside>
<?php
	$sql = "SELECT DISTINCT subjectName FROM subjectTable";
	$query = mysqli_query($conn,$sql);
	$result = array();
	while ($fetch = mysqli_fetch_assoc($query)) {
		
		$result[] = $fetch;
	}
	foreach ($result as $r) {
		$subject = preg_replace('/\s+/', '', $r);
		$subjectname = $r;
?>
<script type="text/javascript">
	$(document).ready(function(){
		var s = <?php echo json_encode($subject); ?>;
		var sj = <?php echo json_encode($r); ?>;
		var sub = Object.values(s);
		var subname = Object.values(sj);
		var subject = sub[0];
		var subjectname = subname[0];
		$('#subject').click(function(){
			$.ajax({
				url:"../index.php?action=display",
				type:"post",
				data:"sub=subjectname",
				datatype:"html",
				success:function(response){
					$(".subjectclass").empty();
					$(".subjectclass").html(response);
				}
			});
		});
	});
</script>
<?php
}
?>
<aside class="dpanel">
	<table class="subtable"></table>
	<table class="subjectclass"></table>
</aside>
</form> 
</body>
</html>
<?php
} else {
	$url = "admin.php";
	header("Location:$url");
}
?>
