<?php
session_start();
if(isset($_SESSION['admin_login_error'])){
    $login_error = $_SESSION['admin_login_error'];
    unset($_SESSION['admin_login_error']);
}

?>
<html>
    <head>
        <title>Admin Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <link rel="stylesheet" href="../files/css/styles.css" type="text/css">
        <script type="text/javascript" src="../files/js/jquery-3.1.1.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".close_error_div_img").click(function(){
                    $(".login_error_div").fadeOut();
                });
            });
        </script>
    </head>
    <body>
        <?php 
        if(isset($login_error)){
            echo "<div class='login_error_div'>" . $login_error . "<img class='close_error_div_img' src='../files/images/error.png'></div>";
        }      
        ?>
            <form id="admin_login_form" method="post" action="../index.php">
                <table><tr><td><label>User Name</label></td><td><input id="admin_user_name" name="admin_user_name" type="text" placeholder="User Name" size="50"></td></tr>
                <tr><td><label>Password</label></td><td><input id="admin_password" name="admin_password" type="password" placeholder="Password" size="50"></td></tr>
                <tr><td></td><td><input class="login_button" id = "action" name="action" type="submit" value="Admin Login"></td></tr>
                </table>
            </form>
        
        <?php require('../files/footer.php'); ?>
    </body>
</html>