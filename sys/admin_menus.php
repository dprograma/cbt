<?php
if (isset($_SESSION['admin_usr_name']) && isset($_SESSION['admin_pwd'])){
	$admin_username = $_SESSION['admin_usr_name'];
	$admin_password = $_SESSION['admin_pwd'];
}
elseif(isset($_SESSION['teacher_usr_name']) && isset($_SESSION['teacher_pwd'])){
	
	$teacher_username = $_SESSION['teacher_usr_name'];
	$teacher_password = $_SESSION['teacher_pwd'];
	$t_crypt = $_SESSION['teacher_cryption'];
}
else{
	$admin_username = '';
	$admin_password = '';
	$teacher_username = '';
	$teacher_password = '';
	$t_crypt = '';
}
if(isset($admin_username) && isset($admin_password)) {
require('../sys/config/config.php');
$stmt = mysqli_stmt_init($conn);
mysqli_stmt_prepare($stmt,"SELECT adminPrivilege FROM adminTable");
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt,$access);
mysqli_stmt_fetch($stmt);
if($access == 1) {

$admin_nav_menus = array('Set Time Table','Edit Time Table','Print Time Table','Create Teacher','Edit Teacher','Create Student','Edit Student','Change Password','View All Teachers','View All Students','View All Exam','View All Time Tables','Change Local Time','Change Subject','Set Exam Questions','Log Out');

$admin_button_menus = array('Set Time Table','Create Teacher','Create Student','Change Password','View All Teachers','View All Students','View All Exam','View All Time Tables','Change Local Time','Change Subject','Set Exam Questions','Log Out');

mysqli_stmt_free_result($stmt);
mysqli_stmt_close($stmt);
mysqli_close($conn);
	}
}elseif(isset($teacher_username) && isset($teacher_password)) {
			require('../sys/config/config.php');
			$stmt = mysqli_stmt_init($conn);
			mysqli_stmt_prepare($stmt,"SELECT teacherPrivilege FROM teacherTable WHERE `teacherUserName` = '$teacher_username' AND AES_DECRYPT(`teacherPwd`,'$t_crypt') = '$teacher_password'");
			mysqli_stmt_execute($stmt);
			mysqli_stmt_store_result($stmt);
			mysqli_stmt_bind_result($stmt,$access);
			mysqli_stmt_fetch($stmt);

	if ($access == 2) {
		
		$teacher_nav_menus = array('Print Time Table','Change Password','View All Students','View All Exam','View All Time Tables','Change Subject','Set Exam Questions','Edit Exam Questions','Log Out');

		$teacher_button_menus = array('Set Exam Questions','Change Password','View All Students','View All Exam','View All Time Tables','Log Out');

	}
}
?>
<ul class="vert_nav">
<?php
if(isset($admin_username) || isset($admin_password) && $access == 1) {
	foreach($admin_nav_menus as $value){
		if($value == 'Log Out') {
			echo "<li><a href = '../index.php?action=LogOut'>" . $value . "</a></li>";
		}else{
			echo "<li><a href = '" . preg_replace('/\s+/', '_', strtolower($value)) . ".php'>" . $value . "</a></li>";
		}
	}
}
if(isset($teacher_username) || isset($teacher_password) && $access == 2) {
	foreach($teacher_nav_menus as $value){
		if($value == 'Log Out') {
			echo "<li><a href = '../index.php?action=LogOut'>" . $value . "</a></li>";
		}else{
			echo "<li><a href = '" . preg_replace('/\s+/', '_', strtolower($value)) . ".php'>" . $value . "</a></li>";
		}
	}
}
?>
</ul>