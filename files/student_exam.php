<?php
session_start();
date_default_timezone_set('Africa/Lagos');
if (isset($_SESSION['student_Id']) && isset($_SESSION['student_pwd']) && isset($_SESSION['encrypt'])) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Examination</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body class="no_background">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	<div id="demo">
	<span class="hours"></span>&nbsp;:&nbsp;<span class="minutes"></span>&nbsp;:&nbsp;<span class="seconds"></span>
	</div>
<?php
	require('../sys/config/config.php');
	include('../sys/admin_header.php');

	if(isset($_SESSION['classname']) && isset($_SESSION['examname']) && isset($_SESSION['subjectname'])) {
		$class_name = $_SESSION['classname'];
		$exam_name = $_SESSION['examname'];
		$subject_name = $_SESSION['subjectname'];
	}else{
		$class_name = '';
		$exam_name = '';
		$subject_name = '';
	}
	
	$today = date('Y-m-d');
	
	$stmt = mysqli_stmt_init($conn);
	mysqli_stmt_prepare($stmt,"SELECT startTime,stopTime FROM timeTable WHERE subjectName = ? AND className = ? AND examName = ? AND examDay = '$today'");
	mysqli_stmt_bind_param($stmt, 'sss', $subject_name,$class_name,$exam_name);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	mysqli_stmt_bind_result($stmt,$start_time,$stop_time);
	mysqli_stmt_fetch($stmt);
	$row = mysqli_stmt_num_rows($stmt);

	if ($row>0) {
		$start = strtotime($start_time);
		$stop = strtotime($stop_time);
	}else{
		$start = '';
		$stop = '';
	}
	$now = strtotime(date('H:i:s'));
	$stp = date('F j, Y H:i:s',$stop);
	$distance = $stop - $now;
	$redirect = "student_result.php";

	if ($now > $start) {	
?>
	<script type="text/javascript">
	var stp = <?php echo json_encode($stp); ?>;
	var stop = new Date(stp).getTime();
	
	function updateClock(){
		var start = new Date().getTime();
		var distance = stop - start;
		var hours = Math.floor((distance % (1000*60*60*24))/(1000*60*60));
		var minutes = Math.floor((distance % (1000*60*60))/(1000*60));
		var seconds = Math.floor((distance % (1000*60))/1000);

		var clock = document.getElementById("demo");
		var hoursSpan = clock.querySelector('.hours');
		var minutesSpan = clock.querySelector('.minutes');
		var secondsSpan = clock.querySelector('.seconds');
		hoursSpan.innerHTML = hours;
		minutesSpan.innerHTML = minutes;
		secondsSpan.innerHTML = seconds;

		if (distance < 0) {
			clearInterval(x);

			document.getElementById("demo").innerHTML = "EXPIRED!";
			var redirect = <?php echo json_encode($redirect); ?>;
			window.location.replace(redirect);
		}
	}
	updateClock();
	var x = setInterval(updateClock,1000);
</script>
	<div class="questionboard"></div>
	<div class="numberpanel"></div>
</body>
</html>
<?php
	} 
}else {
		$url = 'student_login.php';
		header('location:$url');
}
?>