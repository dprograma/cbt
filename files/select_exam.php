<?php
session_start();

ob_start();
if (isset($_SESSION['student_Id']) && isset($_SESSION['student_pwd']) && isset($_SESSION['encrypt'])) {
	$studentID = $_SESSION['student_Id'];
	$studentpwd = $_SESSION['student_pwd'];
	$s_crypt = $_SESSION['encrypt'];
	
		require('../sys/config/config.php');
		
		$stmt = mysqli_stmt_init($conn);
		mysqli_stmt_prepare($stmt,"SELECT studentName,className FROM studentTable WHERE studentId = ? AND AES_DECRYPT(studentPwd,'$s_crypt') = ?");
		mysqli_stmt_bind_param($stmt, 'ss', $studentID,$studentpwd);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		mysqli_stmt_bind_result($stmt,$student_name,$class_name);
		mysqli_stmt_fetch($stmt);

		$today = date('Y-m-d');
		
		mysqli_stmt_prepare($stmt,"SELECT subjectName,examName,examDay,startTime,stopTime,duration FROM timeTable WHERE className = ? AND examDay = ?");
		mysqli_stmt_bind_param($stmt, 'ss', $class_name,$today);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		mysqli_stmt_bind_result($stmt,$subject_name,$exam_name,$exam_day,$start_time,$stop_time,$duration);
		mysqli_stmt_fetch($stmt);
		
		$row = mysqli_stmt_num_rows($stmt);
	
		if ($row>0) {
			$actualtime = strtotime($start_time);
			$stptm = date("H:i:s", strtotime($stop_time));
			$stopagetime = strtotime($stptm);
			
			$now = strtotime(date("H:i:s"));
			
			$difference = $stopagetime - $now;

			
			$diff = date("h:i:s",$difference);
			
			if ($now > $actualtime || $difference > 0) {
				echo "<div class='preview'>" . "<p>Student Name: " . $student_name . "&nbsp;&nbsp;Student Class: " . $class_name . "<p/>" . "<p>Exam Name: " . $exam_name . "&nbsp;&nbsp;Subject Name: " . $subject_name . "<p/>" .  "<p>Date: " . $exam_day . "&nbsp;Start Time: " . $start_time . "&nbsp;&nbsp;Time Left: " . $diff  . "</p>" . "<form method = 'post' action = '../index.php'><p><input type =  'hidden' name = 'studentname' value = '$student_name'></p>" . "<p><input type = 'hidden' name = 'classname' value = '$class_name'></p>" . "<p><input type = 'hidden' name = 'examname' value = '$exam_name'></p>" . "<p class = 'exambutton'><input type = 'hidden' name = 'subjectname' value = '$subject_name'><input type = 'submit' id = 'action_button' name = 'action' value = 'Start Exam'></p>" . "</form></div>";
				
			}else{
				//Create a session variable for error report
                    $_SESSION['student_login_report'] = "Your Exam cannot begin now. Please wait for Exam period.";
                    $url = "student_login.php";
                    header("location:$url");
			}
		}else{
				//Create a session variable for error report
                    $_SESSION['student_login_report'] = "No Exam available now.";
                    $url = "student_login.php";
                    header("location:$url");
		}
	?>
<!DOCTYPE html>
<html>
<head>
	<title>Student Examination</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
</body>
</html>
<?php
} else {
	$url = "student_login.php";
	header("Location:$url");
}

?>