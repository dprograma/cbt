<?php
session_start();
if(isset($_SESSION['student_login_report'])){
    $login_report = $_SESSION['student_login_report'];
    unset($_SESSION['student_login_report']);
}
if(isset($_SESSION['reg_success_report'])) {
    $success_report = $_SESSION['reg_success_report'];
    unset($_SESSION['reg_success_report']);
}
?>
<html>
    <head>
        <title>Student Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <link rel="stylesheet" href="css/styles.css" type="text/css">
        <script type="text/javascript" src="../files/js/scripts.js"></script> 
        <script type="text/javascript" src="../files/js/jquery-3.3.1.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".close_error_div_img").click(function(){
                    $(".login_error_div").fadeOut();
                });

                $(".login_error_div").fadeIn().delay(5000).fadeOut();

                $(".close_success_div_img").click(function(){
                    $(".reg_success_div").fadeOut();
                });

                $(".reg_success_div").fadeIn().delay(5000).fadeOut();
                
                $("#new_user_reg_checkbox").on('change',function(e){
                    e.preventDefault();
                if(this.checked){
                     $("#td1").append('<label id="appended_label">Re-enter Password</label>');
                     $("#td2").append('<input class="appended_row" type="password" name="reenter_password" size="35" placeholder="Re-enter password">');
                     var response = $("#student_login_form").find('input[type=submit]');
                     response.replaceWith('<input type="submit" name="action" id="action" value="Register">');
                }
                else{
                    $(".appended_row").remove();
                    $("#appended_label").remove();
                    var response = $("#student_login_form").find('input[type=submit]');
                     response.replaceWith('<input type="submit" name="action" id="action" value="Login">');
                }
            });
            });

        </script>
    </head>
    <body>
        <?php 
        require('../files/header.php');
        if(isset($login_report)) {
            echo "<div class='login_error_div'>" . $login_report . "<img class='close_error_div_img' src='images/error.png'></div>";
        }  
        if(isset($success_report)) {
            echo "<div class='reg_success_div'>" . $success_report . "<img class='close_success_div_img' src='images/success.png'></div>";
        }        
        ?>
        
            <form id="student_login_form" method="post" action="../index.php">
                <table><tr><td><label>Student ID</label></td><td><input id="student_Id" name="student_Id" type="text" placeholder="Student ID" size="35"></td><td></td></tr>
                <tr><td><label>Password</label></td><td><input id="student_password" name="student_password" type="password" placeholder="Password" size="35"></td><td><input type="checkbox" name="new_user_reg_checkbox" id="new_user_reg_checkbox">&nbsp;<label style="font-size: 14px;">New Student?</label></td></tr>
                <tr><td id="td1"></td><td id="td2"></td><td><input id="action" name="action" type="submit" value="Login"></td></tr>
                </table>
            </form>
        
        <?php require('../files/footer.php')?>
    </body>
</html>

