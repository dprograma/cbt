<?php

$year = "2018";
$class = "JSS1";
$subject = "agricscience";
$sql = "CREATE TABLE IF NOT EXISTS " . $year . "_" . $class . "_" . $subject . "_questionTable(
            questionId INT(10) NOT NUL auto_increment,
            term VARCHAR(150) NOT NULL,
            question TEXT NOT NULL,
            optionA VARCHAR(150) NOT NULL,
            optionB VARCHAR(150) NOT NULL,
            optionC VARCHAR(150) NOT NULL,
            optionD VARCHAR(150) NOT NULL,
            optionE VARCHAR(150) NOT NULL,
            PRIMARY KEY(questionId)
            )ENGINE = InnoDB;";

 echo $sql;
?>