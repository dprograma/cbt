<?php
	class MyClass
	{
		public $prop1 = "This is my PHP Script!";

		public function setproperty($newval)
		{
			$this->prop1 = $newval;
		}
		public function getproperty()
		{
			return $this->prop1; 
		}
	}
	$obj = new MyClass();
	
	echo $obj->getproperty();
	$obj->setproperty("I am a new property!");
	echo "<br/>";
	echo $obj->getproperty();
?>