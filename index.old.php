<?php
//Ensure form request is sent
if(isset($_SERVER['REQUEST_METHOD'])){
    
    switch($_REQUEST['action']){
        case 'Admin Reg':
            require('sys/config/config.php');

            $admin_reg_username = trim(strip_tags($_POST['admin_reg']));
            $admin_reg_password = trim(strip_tags($_POST['admin_password_reg']));

            $crypt = openssl_random_pseudo_bytes(150);
            
            //Insert admin username and password in Admin table
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"INSERT IGNORE INTO adminTable(adminPwd,encrypt,adminUserName)VALUES(AES_ENCRYPT('$admin_reg_password','$crypt'),'$crypt',AES_ENCRYPT('$admin_reg_username','$crypt'))");

            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            mysqli_close($conn);
            break;
            exit();
        case 'Admin Login':
            require('sys/config/config.php');

            $admin_username = trim(strip_tags($_POST['admin_user_name']));
            $admin_pwd = trim(strip_tags($_POST['admin_password']));
        
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,'SELECT encrypt FROM adminTable');

            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$crypt);
            mysqli_stmt_fetch($stmt);

            $stmti = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmti,"SELECT AES_DECRYPT(adminPwd,'$crypt'),AES_DECRYPT(adminUserName,'$crypt') FROM adminTable");

            mysqli_stmt_execute($stmti);
            mysqli_stmt_store_result($stmti);
            mysqli_stmt_bind_result($stmti,$admin_pass_word,$admin_user_name);
            mysqli_stmt_fetch($stmti);
            $row = mysqli_stmt_num_rows($stmt);

            if($admin_user_name == $admin_username || $admin_pass_word == $admin_pwd){
                //Create session variables for admin
                session_start();
                ob_start();
                session_regenerate_id();
                $_SESSION['admin'] = sha1($_SERVER['HTTP_USER_AGENT']);
                $_SESSION['admin_usr_name'] = $admin_user_name;
                $_SESSION['admin_pwd'] = $admin_pass_word;
                $_SESSION['cryption']   = $crypt;
                
                //redirect to admin dashboard
                $url = "sys/admin_dashboard.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_stmt_close($stmti);
                mysqli_close($conn);
            }else{
                session_start();
                ob_start();
                $_SESSION['admin_login_error'] = "Invalid login credentials. Please check your login details and retry.";
                $url = "sys/admin.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_stmt_close($stmti);
                mysqli_close($conn);
            }
            break;
            exit(); 
            case 'Register Teacher':
                require('sys/config/config.php');

                $teacher_username = trim(strip_tags($_POST['teacher_user_name']));
                $teacher_pwd = trim(strip_tags($_POST['teacher_password']));
                $teacher_reenter_pwd = trim(strip_tags($_POST['teacher_reenter_password']));

                $crypt = openssl_random_pseudo_bytes(150);
                
                if ($teacher_pwd == $teacher_reenter_pwd  && !empty($teacher_pwd) && !empty($teacher_username)) {

                    $stmt = mysqli_stmt_init($conn);
                    mysqli_stmt_prepare($stmt,"INSERT IGNORE INTO teacherTable(teacherUserName,teacherPwd,encrypt) VALUES(?,AES_ENCRYPT($teacher_pwd,'$crypt'),?)");

                    mysqli_stmt_bind_param($stmt, 'ss', $teacher_username,$crypt);

                    mysqli_stmt_execute($stmt);

                    $row = mysqli_stmt_num_rows($stmt);

                    if ($row>0) {
                        //Create a session variable for success report
                        session_start();
                        ob_start();
                        $_SESSION['reg_success_report'] = "Successful registration! Please login.";
                        $url = "files/teacher_login.php";
                        header("location:$url");
                        ob_end_flush();
                        mysqli_stmt_close($stmt);
                        mysqli_close($conn);
                    }else{
                        //Create a session variable for success report
                        session_start();
                        ob_start();
                        $_SESSION['reg_success_report'] = "You are already registered! Please login.";
                        $url = "files/teacher_login.php";
                        header("location:$url");
                        ob_end_flush();
                        mysqli_stmt_close($stmt);
                        mysqli_close($conn);
                    }
                }else{
                    session_start();
                    ob_start();
                    $_SESSION['teacher_login_error'] = "Invalid credentials. Please check your details and retry.";
                    $url = "sys/create_teacher.php";
                    header("location:$url");
                    ob_end_flush();
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn);
                }
                break;
                exit();
        case 'Teacher Login':
            require('sys/config/config.php');

            $teacher_username = trim(strip_tags($_POST['teacher_user_name']));
            $teacher_pwd = trim(strip_tags($_POST['teacher_password']));
        
            $stmt = mysqli_stmt_init($conn);
            
            mysqli_stmt_prepare($stmt,"SELECT encrypt FROM teacherTable WHERE `teacherUserName` = ?");

            mysqli_stmt_bind_param($stmt, 's', $teacher_username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt,$crypt);
            mysqli_stmt_fetch($stmt);

            mysqli_stmt_prepare($stmt,"SELECT AES_DECRYPT(teacherPwd,'$crypt') FROM teacherTable WHERE `teacherUserName` = ?");

            mysqli_stmt_bind_param($stmt, 's', $teacher_username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt,$password_decrypt);
            mysqli_stmt_fetch($stmt);

            if($teacher_pwd == $password_decrypt){
                //Create session variables for teacher
                session_start();
                ob_start();
                session_regenerate_id();
                $_SESSION['teacher'] = sha1($_SERVER['HTTP_USER_AGENT']);
                $_SESSION['teacher_usr_name'] = $teacher_username;
                $_SESSION['teacher_pwd'] = $password_decrypt;
                $_SESSION['teacher_cryption']   = $crypt;
                
                //redirect to teacher dashboard
                $url = "sys/admin_dashboard.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                }
                else{
                session_start();
                ob_start();
                $_SESSION['teacher_login_error'] = "Invalid login credentials. Please check your login details and retry.";
                $url = "files/teacher_login.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                }
            break;
            exit();
        case 'Register':
            require('sys/config/config.php');

            $student_ID = trim(strip_tags($_POST['student_Id']));
            $student_pwd = trim(strip_tags($_POST['student_password']));
            $student_reenter_pwd = trim(strip_tags($_POST['reenter_password']));

            if($student_pwd == $student_reenter_pwd && !empty($student_pwd)){

                $stmt = mysqli_stmt_init($conn);
                mysqli_stmt_prepare($stmt,"SELECT encrypt FROM studentTable WHERE `studentId` = ?");

                mysqli_stmt_bind_param($stmt, 's', $student_ID);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt,$crypt);
                mysqli_stmt_fetch($stmt);

                mysqli_stmt_prepare($stmt,"SELECT studentId,AES_DECRYPT(studentPwd,'$crypt'),encrypt FROM studentTable WHERE `studentId` = ? AND `studentPwd` = ?");

                mysqli_stmt_bind_param($stmt, 'ss', $student_ID,$student_pwd);
                mysqli_stmt_execute($stmt);
                $row = mysqli_stmt_num_rows($stmt);

                if($row>0){
                    //Create a session variable for report
                    session_start();
                    ob_start();
                    $_SESSION['reg_success_report'] = "You are already a registered student! Please login.";
                    $url = "files/student_login.php";
                    header("location:$url");
                    ob_end_flush();
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn);
                }
                else{

                    $stmt = mysqli_stmt_init($conn);
                    mysqli_stmt_prepare($stmt,"SELECT studentPwd,encrypt FROM studentTable WHERE `studentId` = ?");
                    mysqli_stmt_bind_param($stmt, 's', $student_ID);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_store_result($stmt);
                    mysqli_stmt_bind_result($stmt,$studentpwd,$cryption);
                    $row = mysqli_stmt_num_rows($stmt);
                    mysqli_stmt_fetch($stmt);

                    if($row>0) {

                        if(empty($studentpwd) || empty($cryption)){

                        $cryption = openssl_random_pseudo_bytes(150);

                        //Insert encryption and password in student table
                        $stmt = mysqli_stmt_init($conn);
                        mysqli_stmt_prepare($stmt,"UPDATE studentTable SET `studentPwd` = AES_ENCRYPT($student_pwd,'$cryption'), `encrypt` = ? WHERE `studentId` = ?");
                        mysqli_stmt_bind_param($stmt, 'ss', $cryption,$student_ID);
                        mysqli_stmt_execute($stmt);

                            if($stmt){

                                //Create a session variable for success report
                                session_start();
                                ob_start();
                                $_SESSION['reg_success_report'] = "Successful registration! Please login.";
                                $url = "files/student_login.php";
                                header("location:$url");
                                ob_end_flush();
                                mysqli_stmt_close($stmt);
                                mysqli_close($conn);
                            }
                        }
                        else{
                            //Create a session variable for report
                            session_start();
                            ob_start();
                            $_SESSION['reg_success_report'] = "You are already a registered student! Please login.";
                            $url = "files/student_login.php";
                            header("location:$url");
                            ob_end_flush();
                            mysqli_stmt_close($stmt);
                            mysqli_close($conn);
                        }
                    }
                    else{
                    //Create a session variable for error report
                    session_start();
                    ob_start();
                    $_SESSION['student_login_report'] = "Invalid credentials. Please check your details and retry.";
                    $url = "files/student_login.php";
                    header("location:$url");
                    ob_end_flush();
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn);
                    }
                }
            }
            else{
            //Create a session variable for error report
            session_start();
            ob_start();
            $_SESSION['student_login_report'] = "Invalid credentials. Please check your details and retry.";
            $url = "files/student_login.php";
            header("location:$url");
            ob_end_flush();
            mysqli_stmt_close($stmt);
            mysqli_close($conn);
            }
            break;
            exit();
        case 'Login':
            require('sys/config/config.php');

            $student_ID = trim(strip_tags($_POST['student_Id']));
            $student_pwd = trim(strip_tags($_POST['student_password']));

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT encrypt FROM studentTable WHERE `studentId` = ?");

            mysqli_stmt_bind_param($stmt, 's', $student_ID);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$crypt);
            mysqli_stmt_fetch($stmt);
            
            $stmti = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmti,"SELECT AES_DECRYPT(studentPwd,'$crypt') FROM studentTable WHERE `studentId` = ?");
            mysqli_stmt_bind_param($stmti, 's', $student_ID);
            mysqli_stmt_execute($stmti);
            mysqli_stmt_store_result($stmti);
            mysqli_stmt_bind_result($stmti,$password_decrypt);
            $row = mysqli_stmt_num_rows($stmti);
            mysqli_stmt_fetch($stmti);
    
            if($row>0){
            //Create session variables for student
            session_start();
            ob_start();
            $_SESSION['student_Id'] = $student_ID;
            $_SESSION['student_pwd'] = $password_decrypt;
            $_SESSION['encrypt'] = $crypt;
            
            //redirect to student area
            $url = "files/select_exam.php";
            header("location:$url");
            ob_end_flush();
            mysqli_stmt_close($stmt);
            mysqli_stmt_close($stmti);
            mysqli_close($conn);
            }
            else{
            //Create a session variable for error report
            session_start();
            ob_start();
            $_SESSION['student_login_report'] = "Invalid login credentials. Please check your login details and retry.";
            $url = "files/student_login.php";
            header("location:$url");
            ob_end_flush();
            mysqli_stmt_close($stmt);
            mysqli_close($conn);
            }
            break;
            exit();
        case 'Start Exam':
            session_start();
            ob_start();
            $student_name = $_POST['studentname'];
            $class_name = $_POST['classname'];
            $exam_name = $_POST['examname'];
            $subject_name = $_POST['subjectname'];

            $_SESSION['studentname'] = $student_name;
            $_SESSION['classname'] = $class_name;
            $_SESSION['examname'] = $exam_name;
            $_SESSION['subjectname'] = $subject_name;

            $url = "files/student_exam.php";
            header("location:$url");
            ob_end_flush();
            break;
            exit();
        case 'ShowResult':
            session_start();
            if (isset($_SESSION['student_Id']) || isset($_SESSION['student_pwd'])) {
                unset($_SESSION['student_Id']);
                unset($_SESSION['student_pwd']);
                unset($_SESSION['studentname']);
                unset($_SESSION['classname']);
                unset($_SESSION['examname']);
                unset($_SESSION['subjectname']); 
                session_destroy($_SESSION['student_Id']);
                session_destroy($_SESSION['student_pwd']);
                session_destroy($_SESSION['studentname']);
                session_destroy($_SESSION['classname']);
                session_destroy($_SESSION['examname']);
                session_destroy($_SESSION['subjectname']);
                $url = "sys/student_result.php";
                header("location:$url");
            }
            break;
            exit();
        case 'Submit':
            if(isset($_POST['classname']) && isset($_POST['selectexam'])){
            date_default_timezone_set('UTC');
            require('sys/config/config.php');
            $class_name = $_POST['classname'];
            $exam_name = $_POST['selectexam'];
            $selectdate = $_POST['sl_date'];
            $arr = explode('-', $selectdate);
            $yr = $arr['0'];
            $mth = $arr['1'];
            $d = $arr['2'];
            
            $exam_day = date('Y-m-d',mktime(0,0,0,$mth,$d,$yr));
            
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            while (mysqli_stmt_fetch($stmt)) {
                $subject[] = $subject_name;
            }
            
            foreach ($subject as $value) {
                $stoptime = preg_replace('/\s+/', '_',$value) . "_stop_time";
                $starttime = preg_replace('/\s+/', '_',$value) . "_start_time";
                $start_time = $_POST[$starttime];
                $stop_time = $_POST[$stoptime];
                $start = strtotime($start_time);
                $stop = strtotime($stop_time);
                $sta_time = date('h:i:sa',$start);
                $stp_time = date('h:i:sa',$stop);
                
                if (preg_match('/(0\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d)(AM|PM)/i', $sta_time) && preg_match('/(0\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d)(AM|PM)/i', $stp_time)) {
                $diff = $stop-$start;
                $duration = date('h:i:s',$diff);

                mysqli_stmt_prepare($stmt,"INSERT INTO timeTable VALUES(NULL,'$value','$class_name','$exam_name','$exam_day','$sta_time','$stp_time','$duration',NULL,NULL)");
                mysqli_stmt_execute($stmt);
                $row = mysqli_stmt_num_rows($stmt);
                    if(mysqli_insert_id($conn)){
                        //Create a session variable for report
                        session_start();
                            $_SESSION['success_report'] = "Successful Entry!";
                            echo $_SESSION['success_report'];
                            $url = "sys/set_time_table.php";
                            header("location:$url");
                    }

                }elseif (empty($sta_time) || empty($stp_time)) {
                    session_start();
                        $_SESSION['success_report'] = "Successful Entry. Empty values entered!";
                        echo $_SESSION['success_report'];
                        $url = "sys/set_time_table.php";
                        header("location:$url");
                }
                else{
                    session_start();
                    $_SESSION['error_report'] = "Invalid time entryME!";
                    $url = "sys/set_time_table.php";
                   header("location:$url");
                }
                
            }
        }
            break;
            break;
            exit();
        case 'JSS1':
            require('sys/config/config.php');
            date_default_timezone_set('UTC');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><label style = 'text-align:right;'>" . $subject_name . "</label></td>" . "<td>Start Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . preg_replace('/\s+/', '_',$subject_name) . "_start_time' value = '' step = '2'></td><td>Stop Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . $subject_name . "_stop_time' value = '' step = '2'></td></tr>";
                    }
            echo "<tr><td><input type='hidden' name='classname' value='$class_name'></td><td><input type='submit' name='action' value='Submit'></td></tr>";
            break;
            exit();
            case 'JSS2':
            require('sys/config/config.php');
            date_default_timezone_set('UTC');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><label style = 'text-align:right;'>" . $subject_name . "</label></td>" . "<td>Start Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . preg_replace('/\s+/', '_',$subject_name) . "_start_time' value = '' step = '2'></td><td>Stop Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . $subject_name . "_stop_time' value = '' step = '2'></td></tr>";
                    }
            echo "<tr><td><input type='hidden' name='classname' value='$class_name'></td><td><input type='submit' name='action' value='Submit'></td></tr>";
            break;
            exit();
            case 'JSS3':
            require('sys/config/config.php');
            date_default_timezone_set('UTC');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><label style = 'text-align:right;'>" . $subject_name . "</label></td>" . "<td>Start Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . preg_replace('/\s+/', '_',$subject_name) . "_start_time' value = '' step = '2'></td><td>Stop Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . $subject_name . "_stop_time' value = '' step = '2'></td></tr>";
                    }
            echo "<tr><td><input type='hidden' name='classname' value='$class_name'></td><td><input type='submit' name='action' value='Submit'></td></tr>";
            break;
            exit();
            case 'SSS1':
            require('sys/config/config.php');
            date_default_timezone_set('UTC');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><label style = 'text-align:right;'>" . $subject_name . "</label></td>" . "<td>Start Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . preg_replace('/\s+/', '_',$subject_name) . "_start_time' value = '' step = '2'></td><td>Stop Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . $subject_name . "_stop_time' value = '' step = '2'></td></tr>";
                    }
            echo "<tr><td><input type='hidden' name='classname' value='$class_name'></td><td><input type='submit' name='action' value='Submit'></td></tr>";
            break;
            exit();
            case 'SSS2':
            require('sys/config/config.php');
            date_default_timezone_set('UTC');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><label style = 'text-align:right;'>" . $subject_name . "</label></td>" . "<td>Start Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . preg_replace('/\s+/', '_',$subject_name) . "_start_time' value = '' step = '2'></td><td>Stop Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . $subject_name . "_stop_time' value = '' step = '2'></td></tr>";
                    }
            echo "<tr><td><input type='hidden' name='classname' value='$class_name'></td><td><input type='submit' name='action' value='Submit'></td></tr>";
            break;
            exit();
            case 'SSS3':
            require('sys/config/config.php');
            date_default_timezone_set('UTC');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><label style = 'text-align:right;'>" . $subject_name . "</label></td>" . "<td>Start Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . preg_replace('/\s+/', '_',$subject_name) . "_start_time' value = '' step = '2'></td><td>Stop Time&nbsp;&nbsp;<input class = 'subjectinput' type = 'time' name = '" . $subject_name . "_stop_time' value = '' step = '2'></td></tr>";
                    }
            echo "<tr><td><input type='hidden' name='classname' value='$class_name'></td><td><input type='submit' name='action' value='Submit'></td></tr>";
            break;
            exit();
        case 'Logout':
            session_start();

            if (isset($_SESSION['admin_usr_name']) || isset($_SESSION['admin_pwd']) || isset($_SESSION['admin'])) {
                unset($_SESSION['admin_usr_name']);
                unset($_SESSION['admin_pwd']);
                unset($_SESSION['admin']);
                session_destroy($_SESSION['admin_usr_name']);
                session_destroy($_SESSION['admin_pwd']);
                session_destroy($_SESSION['admin']);
                $url = "sys/admin.php";
                header("location:$url");
            }
            elseif (isset($_SESSION['teacher_usr_name']) || isset($_SESSION['teacher_pwd']) || isset($_SESSION['teacher'])) {
                unset($_SESSION['teacher_usr_name']);
                unset($_SESSION['teacher_pwd']);
                unset($_SESSION['teacher']);
                session_destroy($_SESSION['teacher_usr_name']);
                session_destroy($_SESSION['teacher_pwd']);
                session_destroy($_SESSION['teacher']);
                $url = "files/teacher_login.php";
                header("location:$url");
            }
            elseif (isset($_SESSION['student_Id']) || isset($_SESSION['student_pwd'])) {
                unset($_SESSION['student_Id']);
                unset($_SESSION['student_pwd']);
                unset($_SESSION['studentname']);
                unset($_SESSION['classname']);
                unset($_SESSION['examname']);
                unset($_SESSION['subjectname']); 
                session_destroy($_SESSION['student_Id']);
                session_destroy($_SESSION['student_pwd']);
                session_destroy($_SESSION['studentname']);
                session_destroy($_SESSION['classname']);
                session_destroy($_SESSION['examname']);
                session_destroy($_SESSION['subjectname']);
                $url = "files/student_login.php";
                header("location:$url");
            }
            else{
                $url = "files/student.php";
                header("location:$url");
            }
            break;
            exit();
        default:
            $url = "files/student_login.php";
            header("location:$url");
    }
}
else{
    $url = "files/student_login.php";
            header("location:$url");
}
?>