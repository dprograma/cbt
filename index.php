<?php
//Ensure form request is sent
if(isset($_SERVER['REQUEST_METHOD'])){
    
    switch($_REQUEST['action']){
        case 'Admin Reg':
            require('sys/config/config.php');

            $admin_reg_username = trim(strip_tags($_POST['admin_reg']));
            $admin_reg_password = trim(strip_tags($_POST['admin_password_reg']));

            $crypt = openssl_random_pseudo_bytes(150);
            
            //Insert admin username and password in Admin table
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"INSERT IGNORE INTO adminTable(adminPwd,encrypt,adminUserName)VALUES(AES_ENCRYPT('$admin_reg_password','$crypt'),'$crypt',AES_ENCRYPT('$admin_reg_username','$crypt'))");

            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            mysqli_close($conn);
            break;
            exit();
        case 'Admin Login':
            require('sys/config/config.php');

            $admin_username = trim(strip_tags($_POST['admin_user_name']));
            $admin_pwd = trim(strip_tags($_POST['admin_password']));
        
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,'SELECT encrypt FROM adminTable');

            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$crypt);
            mysqli_stmt_fetch($stmt);

            $stmti = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmti,"SELECT AES_DECRYPT(adminPwd,'$crypt'),AES_DECRYPT(adminUserName,'$crypt') FROM adminTable");

            mysqli_stmt_execute($stmti);
            mysqli_stmt_store_result($stmti);
            mysqli_stmt_bind_result($stmti,$admin_pass_word,$admin_user_name);
            mysqli_stmt_fetch($stmti);
            $row = mysqli_stmt_num_rows($stmt);

            if($admin_user_name == $admin_username && $admin_pass_word == $admin_pwd){
                //Create session variables for admin
                session_start();
                ob_start();
                session_regenerate_id();
                $_SESSION['admin'] = sha1($_SERVER['HTTP_USER_AGENT']);
                $_SESSION['admin_usr_name'] = $admin_user_name;
                $_SESSION['admin_pwd'] = $admin_pass_word;
                $_SESSION['cryption']   = $crypt;
                
                //redirect to admin dashboard
                $url = "sys/admin_dashboard.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_stmt_close($stmti);
                mysqli_close($conn);
            }else{
                session_start();
                ob_start();
                $_SESSION['admin_login_error'] = "Invalid login credentials. Please check your login details and retry.";
                $url = "sys/admin.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_stmt_close($stmti);
                mysqli_close($conn);
            }
            break;
            exit(); 
            case 'Register Teacher':
                require('sys/config/config.php');

                $teacher_username = trim(strip_tags($_POST['teacher_user_name']));
                $teacher_pwd = trim(strip_tags($_POST['teacher_password']));
                $teacher_reenter_pwd = trim(strip_tags($_POST['teacher_reenter_password']));

                $crypt = openssl_random_pseudo_bytes(150);
                
                if ($teacher_pwd == $teacher_reenter_pwd  && !empty($teacher_pwd) && !empty($teacher_username)) {

                    $stmt = mysqli_stmt_init($conn);
                    mysqli_stmt_prepare($stmt,"INSERT IGNORE INTO teacherTable(teacherUserName,teacherPwd,encrypt) VALUES(?,AES_ENCRYPT($teacher_pwd,'$crypt'),?)");

                    mysqli_stmt_bind_param($stmt, 'ss', $teacher_username,$crypt);

                    mysqli_stmt_execute($stmt);

                    $row = mysqli_stmt_num_rows($stmt);

                    if ($row>0) {
                        //Create a session variable for success report
                        session_start();
                        ob_start();
                        $_SESSION['reg_success_report'] = "Successful registration! Please login.";
                        $url = "files/teacher_login.php";
                        header("location:$url");
                        ob_end_flush();
                        mysqli_stmt_close($stmt);
                        mysqli_close($conn);
                    }else{
                        //Create a session variable for success report
                        session_start();
                        ob_start();
                        $_SESSION['reg_success_report'] = "You are already registered! Please login.";
                        $url = "files/teacher_login.php";
                        header("location:$url");
                        ob_end_flush();
                        mysqli_stmt_close($stmt);
                        mysqli_close($conn);
                    }
                }else{
                    session_start();
                    ob_start();
                    $_SESSION['teacher_login_error'] = "Invalid credentials. Please check your details and retry.";
                    $url = "sys/create_teacher.php";
                    header("location:$url");
                    ob_end_flush();
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn);
                }
                break;
                exit();
        case 'Teacher Login':
            require('sys/config/config.php');

            $teacher_username = trim(strip_tags($_POST['teacher_user_name']));
            $teacher_pwd = trim(strip_tags($_POST['teacher_password']));
        
            $stmt = mysqli_stmt_init($conn);
            
            mysqli_stmt_prepare($stmt,"SELECT encrypt FROM teacherTable WHERE `teacherUserName` = ?");

            mysqli_stmt_bind_param($stmt, 's', $teacher_username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt,$crypt);
            mysqli_stmt_fetch($stmt);

            mysqli_stmt_prepare($stmt,"SELECT AES_DECRYPT(teacherPwd,'$crypt') FROM teacherTable WHERE `teacherUserName` = ?");

            mysqli_stmt_bind_param($stmt, 's', $teacher_username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt,$password_decrypt);
            mysqli_stmt_fetch($stmt);

            if($teacher_pwd == $password_decrypt){
                //Create session variables for teacher
                session_start();
                ob_start();
                session_regenerate_id();
                $_SESSION['teacher'] = sha1($_SERVER['HTTP_USER_AGENT']);
                $_SESSION['teacher_usr_name'] = $teacher_username;
                $_SESSION['teacher_pwd'] = $password_decrypt;
                $_SESSION['teacher_cryption']   = $crypt;
                
                //redirect to teacher dashboard
                $url = "sys/admin_dashboard.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                }
                else{
                session_start();
                ob_start();
                $_SESSION['teacher_login_error'] = "Invalid login credentials. Please check your login details and retry.";
                $url = "files/teacher_login.php";
                header("location:$url");
                ob_end_flush();
                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                }
            break;
            exit();
        case 'Register':
            require('sys/config/config.php');

            $student_ID = trim(strip_tags($_POST['student_Id']));
            $student_pwd = trim(strip_tags($_POST['student_password']));
            $student_reenter_pwd = trim(strip_tags($_POST['reenter_password']));

            if($student_pwd == $student_reenter_pwd && !empty($student_pwd)){

                $stmt = mysqli_stmt_init($conn);
                mysqli_stmt_prepare($stmt,"SELECT encrypt FROM studentTable WHERE `studentId` = ?");

                mysqli_stmt_bind_param($stmt, 's', $student_ID);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt,$crypt);
                mysqli_stmt_fetch($stmt);

                mysqli_stmt_prepare($stmt,"SELECT studentId,AES_DECRYPT(studentPwd,'$crypt'),encrypt FROM studentTable WHERE `studentId` = ? AND `studentPwd` = ?");

                mysqli_stmt_bind_param($stmt, 'ss', $student_ID,$student_pwd);
                mysqli_stmt_execute($stmt);
                $row = mysqli_stmt_num_rows($stmt);

                if($row>0){
                    //Create a session variable for report
                    session_start();
                    ob_start();
                    $_SESSION['reg_success_report'] = "You are already a registered student! Please login.";
                    $url = "files/student_login.php";
                    header("location:$url");
                    ob_end_flush();
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn);
                }
                else{

                    $stmt = mysqli_stmt_init($conn);
                    mysqli_stmt_prepare($stmt,"SELECT studentPwd,encrypt FROM studentTable WHERE `studentId` = ?");
                    mysqli_stmt_bind_param($stmt, 's', $student_ID);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_store_result($stmt);
                    mysqli_stmt_bind_result($stmt,$studentpwd,$cryption);
                    $row = mysqli_stmt_num_rows($stmt);
                    mysqli_stmt_fetch($stmt);

                    if($row>0) {

                        if(empty($studentpwd) || empty($cryption)){

                        $cryption = openssl_random_pseudo_bytes(150);

                        //Insert encryption and password in student table
                        $stmt = mysqli_stmt_init($conn);
                        mysqli_stmt_prepare($stmt,"UPDATE studentTable SET `studentPwd` = AES_ENCRYPT($student_pwd,'$cryption'), `encrypt` = ? WHERE `studentId` = ?");
                        mysqli_stmt_bind_param($stmt, 'ss', $cryption,$student_ID);
                        mysqli_stmt_execute($stmt);

                            if($stmt){

                                //Create a session variable for success report
                                session_start();
                                ob_start();
                                $_SESSION['reg_success_report'] = "Successful registration! Please login.";
                                $url = "files/student_login.php";
                                header("location:$url");
                                ob_end_flush();
                                mysqli_stmt_close($stmt);
                                mysqli_close($conn);
                            }
                        }
                        else{
                            //Create a session variable for report
                            session_start();
                            ob_start();
                            $_SESSION['reg_success_report'] = "You are already a registered student! Please login.";
                            $url = "files/student_login.php";
                            header("location:$url");
                            ob_end_flush();
                            mysqli_stmt_close($stmt);
                            mysqli_close($conn);
                        }
                    }
                    else{
                    //Create a session variable for error report
                    session_start();
                    ob_start();
                    $_SESSION['student_login_report'] = "Invalid credentials. Please check your details and retry.";
                    $url = "files/student_login.php";
                    header("location:$url");
                    ob_end_flush();
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn);
                    }
                }
            }
            else{
            //Create a session variable for error report
            session_start();
            ob_start();
            $_SESSION['student_login_report'] = "Invalid credentials. Please check your details and retry.";
            $url = "files/student_login.php";
            header("location:$url");
            ob_end_flush();
            mysqli_stmt_close($stmt);
            mysqli_close($conn);
            }
            break;
            exit();
        case 'Login':
            require('sys/config/config.php');

            $student_ID = trim(strip_tags($_POST['student_Id']));
            $student_pwd = trim(strip_tags($_POST['student_password']));

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT encrypt FROM studentTable WHERE `studentId` = ?");

            mysqli_stmt_bind_param($stmt, 's', $student_ID);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$crypt);
            mysqli_stmt_fetch($stmt);
            
            $stmti = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmti,"SELECT AES_DECRYPT(studentPwd,'$crypt') FROM studentTable WHERE `studentId` = ?");
            mysqli_stmt_bind_param($stmti, 's', $student_ID);
            mysqli_stmt_execute($stmti);
            mysqli_stmt_store_result($stmti);
            mysqli_stmt_bind_result($stmti,$password_decrypt);
            $row = mysqli_stmt_num_rows($stmti);
            mysqli_stmt_fetch($stmti);
    
            if($row>0){
            //Create session variables for student
            session_start();
            ob_start();
            $_SESSION['student_Id'] = $student_ID;
            $_SESSION['student_pwd'] = $password_decrypt;
            $_SESSION['encrypt'] = $crypt;
            
            //redirect to student area
            $url = "files/select_exam.php";
            header("location:$url");
            ob_end_flush();
            mysqli_stmt_close($stmt);
            mysqli_stmt_close($stmti);
            mysqli_close($conn);
            }
            else{
            //Create a session variable for error report
            session_start();
            ob_start();
            $_SESSION['student_login_report'] = "Invalid login credentials. Please check your login details and retry.";
            $url = "files/student_login.php";
            header("location:$url");
            ob_end_flush();
            mysqli_stmt_close($stmt);
            mysqli_close($conn);
            }
            break;
            exit();
        case 'Start Exam':
            session_start();
            ob_start();
            if (isset($_POST['studentname']) && isset($_POST['classname']) && isset($_POST['examname']) && isset($_POST['subjectname'])) {
                $student_name = $_POST['studentname'];
                $class_name = $_POST['classname'];
                $exam_name = $_POST['examname'];
                $subject_name = $_POST['subjectname'];
            }

            $_SESSION['studentname'] = $student_name;
            $_SESSION['classname'] = $class_name;
            $_SESSION['examname'] = $exam_name;
            $_SESSION['subjectname'] = $subject_name;

            $url = "files/student_exam.php";
            header("location:$url");
            ob_end_flush();
            break;
            exit();
        case 'ShowResult':
            session_start();
            if (isset($_SESSION['student_Id']) || isset($_SESSION['student_pwd'])) {
                unset($_SESSION['student_Id']);
                unset($_SESSION['student_pwd']);
                unset($_SESSION['studentname']);
                unset($_SESSION['classname']);
                unset($_SESSION['examname']);
                unset($_SESSION['subjectname']); 
                session_destroy($_SESSION['student_Id']);
                session_destroy($_SESSION['student_pwd']);
                session_destroy($_SESSION['studentname']);
                session_destroy($_SESSION['classname']);
                session_destroy($_SESSION['examname']);
                session_destroy($_SESSION['subjectname']);
                $url = "sys/student_result.php";
                header("location:$url");
            }
            break;
            exit();
        case 'display':
        	session_start();
        	if(isset($_POST['sub'])) {
        		$subjectname = $_POST['sub'];
        	}else{
        		$subjectname = 'unavail_able';
        	}
        	if (isset($_SESSION['class_name'])) {
        		$classname = $_SESSION['class_name'];
        		unset($_SESSION['$class_name']);
        	}else{
        		$classname = 'unavailable';
        	}
        	
        	echo "<tr><td colspan = '2' align = 'center'>" . 
        	str_replace('_', ' ',$subjectname) . "</td></tr><tr><td><label>Start&nbsp;&nbsp;&nbsp;</lable><input type = 'time' name = 'starttime' step = '2' pattern = '/(0\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d)(AM|PM)/i' required></td><td>
        	<label>Stop&nbsp;&nbsp;&nbsp;</lable><input type = 'time' name = 'stoptime' step = '2' pattern = '/(0\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d)(AM|PM)/i' required></td></tr><tr><td><input type = 'hidden' name = 'class_name' value = '$classname' ></td><td><input type = 'hidden' name = 'subject_name' value = '$subjectname'></td></tr><tr><td colspan = '2' align = 'center'>
        	<input type = 'submit' name = 'action' value = 'Submit'></td></tr>";

        	break;
        	exit();
        case 'tabs':
            require('sys/config/config.php');
            if (isset($_POST['num']) || isset($_POST['year']) || isset($_POST['class']) || isset($_POST['subject'])) {
                $num = $_POST['num'];
                $year = $_POST['year'];
                $class = str_replace(' ', '_', $_POST['class']);
                $subject = str_replace(' ', '_', $_POST['subject']);
            }else{
                $num = '';
                $year = '';
                $class = '';
                $subject = '';
            }

            $sql = "CREATE TABLE IF NOT EXISTS " . $year . "_" . $class . "_" . $subject . "_questionTable(
            questionId INT(10),
            term VARCHAR(150), 
            question TEXT,
            optionA TEXT,
            optionB TEXT,
            optionC TEXT,
            optionD TEXT,
            optionE TEXT,
            answer CHAR,
            PRIMARY KEY(questionId)
            )ENGINE = InnoDB;";
            mysqli_query($conn,$sql);

            $stmt = mysqli_stmt_init($conn);

            $select = "SELECT questionId FROM " . $year . "_" . $class . "_" . $subject . "_questionTable";
            mysqli_stmt_prepare($stmt,$select);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$id);
            
            if ($id == NULL) {
                $id = 0;
            }
            $val = $id + 1;
            echo "<form method = 'post' class = 'questionform'>";
            echo "<table><tr><td colspan = '2'><label style = 'left: -7px; line-height: 18px;' for = 'questionbox'>" . "Question " . $val . " of " . $num . "</label>&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' id = 'imagequestion' name = 'imagequestion'  class = 'fileupload' onChange = 'uploadquestion();'></div><input id = 'img1' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examquestions' name = 'questionbox' id = 'questionbox'  placeholder = 'Enter text Question...' required></textarea></td></tr>";
            echo "<tr><td><label for = 'optionA'>option A</label>&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionA' id = 'imageoptionA' class = 'fileupload' onChange = 'uploadoptionone();'></div><input id = 'img2' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionA' id = 'optionA' required placeholder = 'Enter text Question...'></textarea></td><td><label for = 'optionB'>option B</label>&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionB' id = 'imageoptionB' class = 'fileupload' onChange = 'uploadoptiontwo();'></div><input id = 'img3' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionB' id = 'optionB' required placeholder = 'Enter text Question...'></textarea></td></tr>";
            echo "<tr><td><label for = 'optionC'>option C</label>&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionC' id = 'imageoptionC' class = 'fileupload' onChange = 'uploadoptionthree();'></div><input id = 'img4' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionC' id = 'optionC' required placeholder = 'Enter text Question...'></textarea></td><td><label for = 'optionD'>option D</label>&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionD' id = 'imageoptionD' class = 'fileupload' onChange = 'uploadoptionfour();'></div><input type id = 'img5' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionD' id = 'optionD' required placeholder = 'Enter text Question...'></textarea></td></tr>";
            echo "<tr><td><label for = 'optionE'>option E</label>&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionE' id = 'imageoptionE' class = 'fileupload' onChange = 'uploadoptionfive();'></div><input id = 'img6' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionE' id = 'optionE' placeholder = 'Enter text Question...'></textarea></td><td><div>Select Option</div>&nbsp;&nbsp;A&nbsp;<input name = 'questOpt' type = 'radio' value = 'A' required></input>&nbsp;B&nbsp;<input name = 'questOpt' type = 'radio' value = 'B' required></input>&nbsp;C&nbsp;<input name = 'questOpt' type = 'radio' value = 'C' required></input>&nbsp;D&nbsp;<input name = 'questOpt' type = 'radio' value = 'D' required></input>&nbsp;E&nbsp;<input name = 'questOpt' type = 'radio' value = 'E' required></input></td></tr>";
            echo "<tr><td colspan = '2' align = 'center'><input type = 'hidden' name = 'year' value = '$year'><input type = 'hidden' name = 'class' value = '$class'><input type = 'hidden' name = 'subject' value = '$subject'><input type = 'hidden' name = 'buttonnumber' value = '$val'><input type = 'hidden' name = 'num' value = '$num'><input type = 'reset' value = 'Reset Question' onClick =  'resetFields();'>&nbsp;&nbsp;<input type = 'submit' name = 'action' value = 'Submit Question' onClick = 'submitquestion(); return false;'></td></tr>";
            echo "</table></form>";

            $rows = 10;
            $cols = $num/$rows;
            echo "<table class = 'tablenum'><tr>";
            for ($i=0; $i < $rows; $i++) { 
                $j = $i*$cols;
               echo "</tr><tr>";
               for ($s=$j; $s < $j+$cols; $s++) { 
                    $p = $s+1;
                   echo "<td><a class = 'numclass' href = '#' onClick = 'QuestionNumber(" . $p . ");'>" . $p . "</a></td>";
               }
            }
            echo "</tr></table>";
            break;
            exit();
        case 'looper':
            require('sys/config/config.php');
            if (isset($_POST['num']) || isset($_POST['year']) || isset($_POST['class']) || isset($_POST['subject'])) {
                $num = $_POST['num'];
                $year = $_POST['year'];
                $class = str_replace(' ', '_', $_POST['class']);
                $subject = str_replace(' ', '_', $_POST['subject']);
            }else{
                $num = '';
                $year = '';
                $class = '';
                $subject = '';
            }

            if (isset($_POST['buttonnumber'])) {
                $btn = $_POST['buttonnumber'];
            }else{
                $btn = '';
            }
            
                if (isset($_POST['questionbox'])) {
                    $questionbox = $_POST['questionbox'];
                }
                elseif(isset($_FILES['imagequestion']['tmp_name'])){

                    $questionbox = $_FILES['imagequestion']['name'];

                }
                else{
                    $questionbox = '';
                }

                if (isset($_POST['optionA'])) {
                    $optionA = $_POST['optionA'];
                }
                elseif(isset($_FILES['imageoptionA']['tmp_name'])){
                    $optionA = $_FILES['imageoptionA']['name'];
                }
                else{
                    $optionA = '';
                }

                if (isset($_POST['optionB'])) {
                    $optionB = $_POST['optionB'];
                }
                elseif(isset($_FILES['imageoptionB']['tmp_name'])){
                    $optionB = $_FILES['imageoptionB']['name'];
                }
                else{
                    $optionB = '';
                }

                if (isset($_POST['optionC'])) {
                    $optionC = $_POST['optionC'];
                }
                elseif(isset($_FILES['imageoptionC']['tmp_name'])){
                    $optionC = $_FILES['imageoptionC']['name'];
                }
                else{
                    $optionC = '';
                }

                if (isset($_POST['optionD'])) {
                    $optionD = $_POST['optionD'];
                }
                elseif(isset($_FILES['imageoptionD']['tmp_name'])){
                    $optionD = $_FILES['imageoptionD']['name'];
                }
                else{
                    $optionD = '';
                }

                if (isset($_POST['optionE'])) {
                    $optionE = $_POST['optionE'];
                }
                elseif(isset($_FILES['imageoptionE']['tmp_name'])){
                    $optionE = $_FILES['imageoptionE']['name'];
                }
                else{
                    $optionE = '';
                }

                if (isset($_POST['questOpt'])) {
                    $answer = $_POST['questOpt'];
                }
                else{
                    $answer = '';
                }
                
                $stmt = mysqli_stmt_init($conn);

                if (isset($_POST['sbf'])) {
                    mysqli_stmt_prepare($stmt,"INSERT INTO `" . $year . "_" . $class . "_" . $subject . "_questionTable` (questionId,term,question,optionA,optionB,optionC,optionD,optionE,answer)VALUES('$btn','$year','$questionbox','$optionA','$optionB','$optionC','$optionD','$optionE','$answer') ON DUPLICATE KEY UPDATE term = '$year',question = '$questionbox',optionA = '$optionA',optionB = '$optionB',optionC = '$optionC',optionD = '$optionD',optionE = '$optionE',answer = '$answer'");
                    mysqli_stmt_execute($stmt);
                }
            
            $stmt2 = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt2,"SELECT question,optionA,optionB,optionC,optionD,optionE,answer FROM `" . $year . "_" . $class . "_" . $subject . "_questionTable` WHERE questionId = '" . $btn . "'");
            mysqli_stmt_execute($stmt2);
            mysqli_stmt_store_result($stmt2);
            mysqli_stmt_bind_result($stmt2,$qst,$optA,$optB,$optC,$optD,$optE,$ans);
            $row = mysqli_stmt_affected_rows($stmt2);
            mysqli_stmt_fetch($stmt2);

            echo "<form method = 'post' class = 'questionform'>";
            echo "<table><tr><td colspan = '2'><label style = 'left: -7px; line-height: 18px;' for = 'questionbox'>" . "Question " . $btn . " of " . $num . "</label>&nbsp;&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' id = 'imagequestion' name = 'imagequestion'  class = 'fileupload' onChange = 'uploadquestion();'></div><input id = 'img1' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examquestions' name = 'questionbox' id = 'questionbox' placeholder = 'Enter text Question...' required>" . $qst . "</textarea></td></tr>";
            echo "<tr><td><label for = 'optionA'>option A</label>&nbsp;&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionA' id = 'imageoptionA' class = 'fileupload' onChange = 'uploadoptionone();'></div><input id = 'img2' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionA' id = 'optionA' placeholder = 'Enter text Question...' required>" . $optA . "</textarea></td><td><label for = 'optionB'>option B</label>&nbsp;&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionB' id = 'imageoptionB' class = 'fileupload' onChange = 'uploadoptiontwo();'></div><input id = 'img3' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionB' id = 'optionB' placeholder = 'Enter text Question...' required>" . $optB . "</textarea></td></tr>";  
            echo "<tr><td><label for = 'optionC'>option C</label>&nbsp;&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionC' id = 'imageoptionC' class = 'fileupload' onChange = 'uploadoptionthree();'></div><input id = 'img4' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionC' id = 'optionC' placeholder = 'Enter text Question...' required>" . $optC . "</textarea></td><td><label for = 'optionD'>option D</label>&nbsp;&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionD' id = 'imageoptionD' class = 'fileupload' onChange = 'uploadoptionfour();'></div><input type id = 'img5' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionD' id = 'optionD' placeholder = 'Enter text Question...' required>" . $optD . "</textarea></td></tr>";
            echo "<tr><td><label for = 'optionE'>option E</label>&nbsp;&nbsp;<div class = 'filebuttonwrap'><span>Upload Image</span><input type = 'file' name = 'imageoptionE' id = 'imageoptionE' class = 'fileupload' onChange = 'uploadoptionfive();'></div><input id = 'img6' class = 'uploadedwrap' disabled = 'disabled' placeholder = 'Select Image...'><textarea class = 'examoptions' name = 'optionE' id = 'optionE' placeholder = 'Enter text Question...'>" . $optE . "</textarea></td><td><div>Select Option</div>&nbsp;A&nbsp;<input name = 'questOpt' type = 'radio' value = 'A' required></input>&nbsp;B&nbsp;<input name = 'questOpt' type = 'radio' value = 'B' required></input>&nbsp;C&nbsp;<input name = 'questOpt' type = 'radio' value = 'C' required></input>&nbsp;D&nbsp;<input name = 'questOpt' type = 'radio' value = 'D' required></input>&nbsp;E&nbsp;<input name = 'questOpt' type = 'radio' value = 'E' required></input></td></tr>";
            echo "<tr><td colspan = '2' align = 'center'><input type = 'hidden' name = 'year' value = '$year'><input type = 'hidden' name = 'class' value = '$class'><input type = 'hidden' name = 'subject' value = '$subject'><input type = 'hidden' name = 'buttonnumber' value = '$btn'><input type = 'hidden' name = 'num' value = '$num'><input type = 'reset' value = 'Reset Question' onClick =  'resetFields();'>&nbsp;&nbsp;<input type = 'submit' name = 'action' value = 'Submit Question' onClick = 'submitquestion(); return false;'></td></tr>";
            echo "</table></form>";

            $rows = 10;
            $cols = $num/$rows;
            echo "<table class = 'tablenum'><tr>";
            for ($i=0; $i < $rows; $i++) { 
                $j = $i*$cols;
               echo "</tr><tr>";
               for ($s=$j; $s < $j+$cols; $s++) { 
                    $p = $s+1;
                   echo "<td><a class = 'numclass' href = '#' onClick = 'QuestionNumber(" . $p . ");'>" . $p . "</a></td>";
               }
            }
            echo "</tr></table>";
            
            break;
            exit();
        case 'Submit':
        	session_start();
            if(isset($_POST['class_name']) && isset($_POST['selectexam'])){
            date_default_timezone_set('UTC');
            require('sys/config/config.php');
            $classname = $_POST['class_name'];
            $subjectname = $_POST['subject_name'];
            $exam_name = $_POST['selectexam'];
            $selectdate = $_POST['sl_date'];
            $arr = explode('-', $selectdate);
            $yr = $arr['0'];
            $mth = $arr['1'];
            $d = $arr['2'];
            
            $exam_day = date('Y-m-d',mktime(0,0,0,$mth,$d,$yr));
        
            $start_time = $_POST['starttime'];
            $stop_time = $_POST['stoptime'];
            $start = strtotime($start_time);
            $stop = strtotime($stop_time);
            $sta_time = date('h:i:sa',$start);
            $stp_time = date('h:i:sa',$stop);
                
                if (preg_match('/(0\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d)(AM|PM)/i', $sta_time) && preg_match('/(0\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d)(AM|PM)/i', $stp_time)) {
                $diff = $stop-$start;
                $duration = date('h:i:s',$diff);
                $sta_tm = date("H:i:s", strtotime($sta_time));
                $stp_tm = date("H:i:s", strtotime($stp_time));

                $stmt = mysqli_stmt_init($conn);
                mysqli_stmt_prepare($stmt,"INSERT INTO timeTable VALUES(NULL,'$subjectname','$classname','$exam_name','$exam_day','$sta_tm','$stp_tm','$duration',NULL,NULL)");
                mysqli_stmt_execute($stmt);
                $row = mysqli_stmt_num_rows($stmt);
                    if(mysqli_insert_id($conn)){
                    	 mysqli_stmt_prepare($stmt,"DELETE FROM timeTable WHERE startTime = '12:00:00' AND duration = '12:00:00'");
                    	 mysqli_stmt_execute($stmt);
                    	 $sql = "UPDATE timeTable SET duration = EXTRACT(MINUTE_SECOND FROM '$duration') WHERE duration > '12:00:00'";
                    	 mysqli_stmt_prepare($stmt,$sql);
                    	 mysqli_stmt_execute($stmt);
                    	 mysqli_stmt_prepare($stmt,"DELETE FROM timeTable WHERE startTime > '$sta_tm' AND className = '$classname' AND subjectName = '$subjectname' AND subjectName = '$subjectname' AND examDay = '$exam_day'");
                    	 mysqli_stmt_execute($stmt);
                        //Create a session variable for success report
                        session_start();
                            $_SESSION['success_report'] = "Successful Entry!";
                            $url = "sys/set_time_table.php";
                            header("location:$url");
                    }
                }
                else{
                    session_start();
                    $_SESSION['error_report'] = "Invalid time entry!";
                    $url = "sys/set_time_table.php";
                   header("location:$url");
                }
        
        }
            break;
            break;
            exit();
        case 'JSS1':
        	session_start();
        	ob_start();
            require('sys/config/config.php');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT DISTINCT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'><tr><td>" . $class_name . "</td></tr>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><a class = 'selectsub' href = '#' onClick = 'getPage(" . json_encode(preg_replace('/\s+/', '_',$subject_name)) . ");' >" . $subject_name . "</a></td></tr>";
                    }
            $_SESSION['class_name'] = $class_name;
            ob_end_flush();
            break;
            exit();
            case 'JSS2':
            session_start();
            ob_start();
            require('sys/config/config.php');
            if(isset($_POST['class'])){
                $class = $_POST['class'];

            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT DISTINCT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'><tr><td>" . $class_name . "</td></tr>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><a class = 'selectsub' href = '#' onClick = 'getPage(" . json_encode(preg_replace('/\s+/', '_',$subject_name)) . ");' >" . $subject_name . "</a></td></tr>";
                    }
            $_SESSION['class_name'] = $class_name;
            ob_end_flush();
            break;
            exit();
            case 'JSS3':
            session_start();
            ob_start();
            require('sys/config/config.php');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'><tr><td>" . $class_name . "</td></tr>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><a class = 'selectsub' href = '#' onClick = 'getPage(" . json_encode(preg_replace('/\s+/', '_',$subject_name)) . ");' >" . $subject_name . "</a></td></tr>";
                    }
            $_SESSION['class_name'] = $class_name;
            ob_end_flush();
            break;
            exit();
            case 'SSS1':
            session_start();
            ob_start();
            require('sys/config/config.php');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'><tr><td>" . $class_name . "</td></tr>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><a class = 'selectsub' href = '#' onClick = 'getPage(" . json_encode(preg_replace('/\s+/', '_',$subject_name)) . ");' >" . $subject_name . "</a></td></tr>";
                    }
            $_SESSION['class_name'] = $class_name;
            ob_end_flush();
            break;
            exit();
            case 'SSS2':
            session_start();
            ob_start();
            require('sys/config/config.php');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'><tr><td>" . $class_name . "</td></tr>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><a class = 'selectsub' href = '#' onClick = 'getPage(" . json_encode(preg_replace('/\s+/', '_',$subject_name)) . ");' >" . $subject_name . "</a></td></tr>";
                    }
            $_SESSION['class_name'] = $class_name;
            ob_end_flush();
            break;
            exit();
            case 'SSS3':
            session_start();
            ob_start();
            require('sys/config/config.php');
            if(isset($_POST['class'])){
                $class = $_POST['class'];
            }
            $class_name = substr_replace($class, " ", 3, 0);
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt,"SELECT subjectName FROM subjectTable WHERE className = ?");
            mysqli_stmt_bind_param($stmt, 's', $class_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            mysqli_stmt_bind_result($stmt,$subject_name);
            echo "<table class='subtable'><tr><td>" . $class_name . "</td></tr>";
            while (mysqli_stmt_fetch($stmt)) {
                echo "<tr><td><a class = 'selectsub' href = '#' onClick = 'getPage(" . json_encode(preg_replace('/\s+/', '_',$subject_name)) . ");' >" . $subject_name . "</a></td></tr>";
                    }
            $_SESSION['class_name'] = $class_name;
            ob_end_flush();
            break;
            exit();
        case 'Logout':
            session_start();

            if (isset($_SESSION['admin_usr_name']) || isset($_SESSION['admin_pwd']) || isset($_SESSION['admin'])) {
                unset($_SESSION['admin_usr_name']);
                unset($_SESSION['admin_pwd']);
                unset($_SESSION['admin']);
                unset($_SESSION['class_name']);
                session_destroy($_SESSION['admin_usr_name']);
                session_destroy($_SESSION['admin_pwd']);
                session_destroy($_SESSION['admin']);
                session_destroy($_SESSION['class_name']);
                $url = "sys/admin.php";
                header("location:$url");
            }
            elseif (isset($_SESSION['teacher_usr_name']) || isset($_SESSION['teacher_pwd']) || isset($_SESSION['teacher'])) {
                unset($_SESSION['teacher_usr_name']);
                unset($_SESSION['teacher_pwd']);
                unset($_SESSION['teacher']);
                session_destroy($_SESSION['teacher_usr_name']);
                session_destroy($_SESSION['teacher_pwd']);
                session_destroy($_SESSION['teacher']);
                $url = "files/teacher_login.php";
                header("location:$url");
            }
            elseif (isset($_SESSION['student_Id']) || isset($_SESSION['student_pwd'])) {
                unset($_SESSION['student_Id']);
                unset($_SESSION['student_pwd']);
                unset($_SESSION['studentname']);
                unset($_SESSION['classname']);
                unset($_SESSION['examname']);
                unset($_SESSION['subjectname']); 
                session_destroy($_SESSION['student_Id']);
                session_destroy($_SESSION['student_pwd']);
                session_destroy($_SESSION['studentname']);
                session_destroy($_SESSION['classname']);
                session_destroy($_SESSION['examname']);
                session_destroy($_SESSION['subjectname']);
                $url = "files/student_login.php";
                header("location:$url");
            }
            else{
                $url = "files/student.php";
                header("location:$url");
            }
            break;
            exit();
        default:
            $url = "files/student_login.php";
            header("location:$url");
    }
}
else{
    ob_start();
    $url = "files/student_login.php";
            header("location:$url");
    ob_end_flush();
}
?>